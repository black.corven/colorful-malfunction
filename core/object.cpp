#include "pch.h"

#include "object.h"
#include "pair.h"

#include <fstream>

using namespace colormalfu;

void Object::write(std::ofstream& of, uint32_t depth)
{
  write_begin(of, depth);

  for (auto& p : m_data)
  {
    p->write(of, depth + 1);
    of << ";\n";
  }

  write_end(of, depth);
}

void Object::write_begin(std::ofstream& of, uint32_t depth)
{
  writeTabs(of, depth);
  of << m_key << " {\n";
}

void Object::write_end(std::ofstream& of, uint32_t depth)
{
  writeTabs(of, depth);
  of << "}";
}