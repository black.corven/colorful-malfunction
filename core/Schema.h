#pragma once

#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

#include "holder.h"
#include "pair.h"
#include "object.h"
#include "group.h"

namespace colormalfu
{
  /**
   * Root class for setheme content
   */
  class Schema : public Holder
  {
  private: 
    std::filesystem::path m_file{};  //!< SETHEME file path
  protected:
    void setPath(std::string path)
    {
      m_file = path;
    }
  public:
    //! Default c'tor
    Schema() : Holder("") {}

    //! Load Schema from SETHEME file
    static std::shared_ptr<Schema> read(std::string file);

    //! Wrapper for Group link
    bool insert(std::weak_ptr<Holder> group_link) { return Holder::insert(group_link); }

    std::weak_ptr<Holder> findHolder(std::string identifier)
    {
      if (auto link = find(identifier); !link.expired())
      {
        if (auto holder = std::dynamic_pointer_cast<Holder>(link.lock()); holder)
        {
          return holder;
        }
      }
      return std::weak_ptr<Holder>();
    }

    //! No identifier for schema
    std::string getIdentifier() = delete;

    std::string getPath()
    {
      return m_file.string();
    }

    //! Write to file using SETHEME format
    void write(fs::path outputFile);

     //! Write schema to stream using SETHEME format
    void write(std::ofstream& of);

  };

  //! Write setheme in strict order
  void write_schema_strict(std::ofstream& of, std::weak_ptr<Schema> link_schema);
}
