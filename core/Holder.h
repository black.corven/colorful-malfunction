#pragma once

#include "BaseProperty.h"

#include <set>
#include <memory>
#include <functional>

namespace colormalfu
{
  /**
   * Class that holds set of propertities
   */
  class Holder : public BaseProperty
  {
  protected:
    //! Collection
    std::set<std::shared_ptr<BaseProperty>, ComparatorProperty_ptr> m_data{};

  public:
    //! Default c'tor
    Holder(std::string identifier) : BaseProperty(identifier) {}

    //! Insert property
    //! TODO: return actual pointer
    bool insert(std::shared_ptr<BaseProperty>&& property)
    {
      if (m_data.find(property->getIdentifier()) == m_data.end())
      {
        m_data.insert(property);
        return true;
      }
      return false;
    }

    //! Insert property by link
    bool insert(std::weak_ptr<BaseProperty> link)
    { 
      if (!link.expired())
      {
        m_data.insert(link.lock());
        return true;
      }
      return false;
    }

    //! Find by identifier
    std::weak_ptr<BaseProperty> find(std::string identifier) const
    {
      if (auto it = m_data.find(identifier); it != m_data.end())
      {
        return *it;
      }
      return std::weak_ptr<BaseProperty>();
    }

    //! Find by list
    std::weak_ptr<BaseProperty> find_recursive(const std::list<std::string>& identifiers) const 
    {
      if (identifiers.empty())
      {
        return std::weak_ptr<BaseProperty>();
      }

      auto holder = this;
      for (auto& id : identifiers)
      {
        auto it = holder->find(id);
        if (it.expired())
        {
          break;  // part not found
        }

        if (*identifiers.rbegin() == it.lock()->getIdentifier())
        {
          return it.lock();  // found
        }

        if (auto tmp = std::dynamic_pointer_cast<Holder>(it.lock()); tmp)
        {
          holder = tmp.get(); // can keep looking
          continue;
        } else {
          break;
        }
      }
      return std::weak_ptr<BaseProperty>();
    }

    void for_each(const std::function<void(std::shared_ptr<BaseProperty>&)> t)
    {
      for (auto elem : m_data)
      {
        t(elem);
      }
    }
  };
}