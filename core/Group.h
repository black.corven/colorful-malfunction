#pragma once

#include "holder.h"

namespace colormalfu
{
  class Pair;
  class Object;

  /**
   * Class represents group of setheme objects
   */
  class Group : public Holder
  {
  public:
    //Default c'tor
    Group(std::string identifier) : Holder(identifier) {};

    //! Find Object Property
    std::weak_ptr<Object> findObject(std::string identifier);

    //! Find Pair Property
    std::weak_ptr<Pair> findPair(std::string identifier);

    // Insert
    //! Insert wrapper for Object link
    bool insert(std::weak_ptr<Object>&& link);

    //! Insert wrapper for Pair link
    bool insert(std::weak_ptr<Pair>&& link);

    void write(std::ofstream& of, uint32_t depth = 0);
    void write_begin(std::ofstream& of, uint32_t depth = 0);
    void write_end(std::ofstream& of, uint32_t depth = 0);
  };
}