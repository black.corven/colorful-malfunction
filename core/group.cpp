#include "pch.h"

#include "Group.h"

#include "pair.h"
#include "object.h"

using namespace colormalfu;

std::weak_ptr<Pair> Group::findPair(std::string identifier)
{
  if (auto link = find(identifier); !link.expired())
  {
    if (auto pair = std::dynamic_pointer_cast<Pair>(link.lock()); pair)
    {
      return pair;
    }
  }
  return std::weak_ptr<Pair>();
}

std::weak_ptr<Object> Group::findObject(std::string identifier)
{
  if (auto link = find(identifier); !link.expired())
  {
    if (auto obj = std::dynamic_pointer_cast<Object>(link.lock()); obj)
    {
      return obj;
    }
  }
  return std::weak_ptr<Object>();
}

bool Group::insert(std::weak_ptr<Object>&& link)
{
  if (!link.expired())
  {
    return Holder::insert(std::dynamic_pointer_cast<BaseProperty>(link.lock()));
  }
  return false;
}

bool Group::insert(std::weak_ptr<Pair>&& link)
{
  if (!link.expired())
  {
    return Holder::insert(std::dynamic_pointer_cast<BaseProperty>(link.lock()));
  }
  return false;
}

void Group::write(std::ofstream& of, uint32_t depth)
{
  write_begin(of);
  for (auto& var : m_data)
  {
    var->write(of, 1);
    of << '\n';
  }
  write_end(of);
}

void Group::write_begin(std::ofstream& of, uint32_t depth)
{
  of << m_key << " {\n";
}

void Group::write_end(std::ofstream& of, uint32_t depth)
{
  of << "}";
}