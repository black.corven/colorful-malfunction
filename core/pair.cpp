#include "pch.h"

#include "pair.h"
#include "object.h"

#include <fstream>
#include <sstream>

#include <iomanip>

using namespace colormalfu;

Pair::Pair(std::string p_key, ValueType p_type, std::string p_value)
  : BaseProperty(p_key)
{
  //TODO: throw or log
  _ASSERT(p_type == ValueType::LITERAL || p_type == ValueType::STRING || p_type == ValueType::IDENTIFIER);
  m_type = p_type;
  if (p_type == ValueType::LITERAL)
  {
    if (p_value != "false" && p_value != "FALSE" && p_value != "False" &&
      p_value != "true" && p_value != "TRUE" && p_value != "True")
    {
      // Unexpected literal
      m_value = "false";
      return;
    }
  }
  m_value = p_value;
}

Pair::Pair(std::string p_key, ValueType p_type, uint32_t p_value)
  : BaseProperty(p_key)
{
  _ASSERT(p_type == ValueType::NUMBER || p_type == ValueType::HEX);
  m_type = p_type;
  m_value = p_value;
}

Pair::Pair(std::string p_key, std::shared_ptr<Object> p_value)
  : BaseProperty(p_key)
{
  m_type = ValueType::OBJECT;
  m_value = p_value;
}

void Pair::setValue(std::string p_value)
{
  switch (m_type)
  {
  case ValueType::LITERAL:
    if (p_value != "false" && p_value != "FALSE" && p_value != "False" &&
        p_value != "true"  && p_value != "TRUE"  && p_value != "True")
    {
      // Unexpected literal
      return;
    }
    [[fallthrough]];
  case ValueType::STRING:
  case ValueType::IDENTIFIER:
    m_value = p_value;
    break;
  case ValueType::NUMBER:
    try
    {
      m_value = std::stoi(p_value);
    }
    catch (std::invalid_argument) {
      // skip
    }
    break;
  case ValueType::HEX:
    try
    {
      m_value = std::stoul(p_value, nullptr, 16);
    }
    catch (std::invalid_argument) {
      // skip
    }
    break;
  }
}

void  Pair::setValue(std::weak_ptr<Object> object_link)
{
  m_type = ValueType::OBJECT;
  m_value = object_link.lock();
}

std::string Pair::getValueString()
{
  switch (m_type)
  {
  case ValueType::IDENTIFIER:
  case ValueType::STRING:
  case ValueType::LITERAL:
    if (std::holds_alternative<std::string>(m_value))
    {
      return std::get<std::string>(m_value);
    }
    break;

  case ValueType::NUMBER:
    if (std::holds_alternative<uint32_t>(m_value))
    {
      return std::to_string((int32_t)std::get<uint32_t>(m_value));
    }
    break;

  case ValueType::HEX:
    if (std::holds_alternative<uint32_t>(m_value))
    {
      std::stringstream stream;
      stream << std::hex << std::uppercase 
        << std::setfill('0') << std::setw(8) 
        << std::get<uint32_t>(m_value);
      std::string result(stream.str());
      return stream.str();
    }
    break;

  case ValueType::OBJECT:
    if (std::holds_alternative<std::shared_ptr<Object>>(m_value))
    {
      return std::get<std::shared_ptr<Object>>(m_value)->getIdentifier();
    }
    break;    
  }

  // TODO: Log or throw if can't get value
  return "undefined";
}

std::weak_ptr<Object> Pair::getObject()
{
  if (m_type == ValueType::OBJECT && std::holds_alternative<std::shared_ptr<Object>>(m_value))
  {
    return std::get<std::shared_ptr<Object>>(m_value);
  }
  return std::weak_ptr<Object>();
}

void Pair::write(std::ofstream& of, uint32_t depth)
{
  writeTabs(of, depth);

  of << m_key << ":";

  switch (m_type)
  {
  case ValueType::OBJECT:
    of << '\n';
    std::get<std::shared_ptr<Object>>(m_value)->write(of, depth++);
    break;
  case ValueType::STRING:
    of << '"' <<getValueString() << '\"';
    break;
  case ValueType::HEX:
    of << "0x";
    [[fallthrough]];
  default:
    of << getValueString();
  }
}

//! Write begin segment
void Pair::write_begin(std::ofstream& of, uint32_t depth)
{
  writeTabs(of, depth);
  of << m_key << ":";
  if (m_type == ValueType::OBJECT)
  {
    of << '\n';
  }
}

//! Write end segment
void Pair::write_end(std::ofstream& of, uint32_t depth)
{
  of << ";\n";
}
