#pragma once

#include <string>
#include <variant>
#include <memory>

#include "BaseProperty.h"

namespace colormalfu
{
  class Object;

  /**
   * Property with value
   */
  class Pair: public BaseProperty
  {
  public:
    enum class ValueType
    {
      NUMBER,               //!< Decimal number (u)i32
      HEX,                  //!< Hex number u32
      IDENTIFIER,           //!< Any std::string match 
      LITERAL,              //!< std::string one of "true" and "false"
      STRING,               //!< Any std::string
      OBJECT                //!< Object
    };

  private:
    ValueType m_type = ValueType::NUMBER;         //!< Pair type
    std::variant<
      uint32_t,
      std::string,
      std::shared_ptr<Object>
    > m_value;                                    //!< Value container

  public:
    //! C'tor by string. 
    //! Make sure p_type one of (LITERAL, STRING, IDENTIFIER)
    Pair(std::string p_key, ValueType p_type, std::string p_value);

    //! C'tor by uint32. 
    //! Make sure p_type one of (NUMBER, HEX)
    Pair(std::string p_key, ValueType p_type, uint32_t p_value);

    //! C'tor by object. 
    Pair(std::string p_key, std::shared_ptr<Object> p_value = nullptr);

    // Getters
    ValueType getType() { return m_type; }

    //! Get string interpretation of value
    std::string getValueString();

    //! Get string interpretation of value
    std::weak_ptr<Object> getObject();

    //! Set value based on ValueType
    void setValue(std::string p_value);

    //! Set object (and change type)
    void setValue(std::weak_ptr<Object> object_link);

    void write(std::ofstream& of, uint32_t depth = 0);
    void write_begin(std::ofstream& of, uint32_t depth = 0);
    void write_end(std::ofstream& of, uint32_t depth = 0);
  };
}