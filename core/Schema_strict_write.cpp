#include "pch.h"

#include "Schema.h"
#include "pair.h"
#include "object.h"

using namespace colormalfu;

//! Node of document tree
struct Order
{
  std::string first;            //!< Identifier
  std::vector<Order>* second;   //!< Fields
};

std::vector<Order> strict_AnimTimeLineControl =
{
  { "Color.AnimTimeLine_EventMark", nullptr },
  { "Color.AnimTimeLine_FrameMark", nullptr },
  { "Color.AnimTimeLine_ImportantKeyframe", nullptr },
  { "Color.AnimTimeLine_KeyframeMark", nullptr },
  { "Color.AnimTimeLine_OffsetFill", nullptr },
  { "Color.AnimTimeLine_Shade", nullptr },
};

std::vector<Order> strict_AnimTrackEditorControl =
{
  { "Color.AnimationClip[CCameraShotClipData]", nullptr },
  { "Color.AnimationClip[CCutsceneCameraClipData]", nullptr },
  { "Color.AnimationClip[CLookTargetClipData]", nullptr },
  { "Color.AnimationClip[CModelMutatorClipData]", nullptr },
  { "Color.AnimationClip[CPlayAnimClipData]", nullptr },
  { "Color.AnimationClip[CPlayCutsceneAnimClipData]", nullptr },
  { "Color.AnimationClip[CPlaySoundClipData]", nullptr },
  { "Color.AnimationClip[CShakerClipData]", nullptr },
  { "Color.AnimationClip[CSubtitlesClipData]", nullptr },
  { "Color.AnimationClip[CVisibilityClipData]", nullptr },
  { "Color.AnimTrackEditor_HeaderBackColor", nullptr },
  { "Color.AnimTrackEditor_TrackBackColor_Active", nullptr },
  { "Color.AnimTrackEditor_TrackBackColor_Focused", nullptr },
  { "Color.AnimTrackEditor_TrackBackColor_Inactive", nullptr },
  { "Color.AnimTrackEditor_TrackBackColor_Muted", nullptr },
  { "Color.AnimTrackEditor_TrackLineColor_Active", nullptr },
  { "Color.AnimTrackEditor_TrackLineColor_Focused", nullptr },
  { "Color.AnimTrackEditor_TrackLineColor_Inactive", nullptr },
  { "Color.AnimTrackEditor_TrackLineColor_Muted", nullptr },
  { "Color.AnimTrackHolder_BackgroundNew", nullptr },
  { "Color.AnimTrackHolder_InactivePlaybackRange", nullptr },
};

std::vector<Order> strict_AssetTrackingSystemExplorer =
{
  { "Color.Background_Button", nullptr },
  { "Color.Background_Even", nullptr },
  { "Color.Background_Group_Heading", nullptr },
  { "Color.Background_Odd", nullptr },
  { "Color.Info_Error", nullptr },
  { "Color.Info_Info", nullptr },
  { "Color.Info_Warning", nullptr },
  { "Color.State_Fixed", nullptr },
  { "Color.State_Needs_Creation", nullptr },
  { "Color.State_Open", nullptr },
};

std::vector<Order> strict_AxisPreview =
{
  { "Color.AxisPreviewBorderColor", nullptr },
  { "Color.AxisPreviewFilterColor", nullptr },
};

std::vector<Order> strict_Borders =
{
  { "Color.Border.M3D_In_LT", nullptr },
  { "Color.Border.M3D_In_LT2", nullptr },
  { "Color.Border.M3D_In_RB", nullptr },
  { "Color.Border.M3D_In_RB2", nullptr },
  { "Color.Border.M3D_Out_LT", nullptr },
  { "Color.Border.M3D_Out_LT2", nullptr },
  { "Color.Border.M3D_Out_RB", nullptr },
  { "Color.Border.M3D_Out_RB2", nullptr },
  { "Color.Border.S3D_In_LT", nullptr },
  { "Color.Border.S3D_In_RB", nullptr },
  { "Color.Border.S3D_Out_LT", nullptr },
  { "Color.Border.S3D_Out_RB", nullptr },
  { "Color.Border.Splitter_LightV", nullptr },
  { "Color.Border.Thin", nullptr },
  { "Color.Border.Thin_3D_LT", nullptr },
  { "Color.Border.Thin_3D_RB", nullptr },
  { "Color.Border.Thin_Bright", nullptr },
  { "Color.Border.Thin_Invisible", nullptr },
};

std::vector<Order> strict_Buttons =
{
  { "Color.Button2_Active.Border_LT", nullptr },
  { "Color.Button2_Active.Border_LT2", nullptr },
  { "Color.Button2_Active.Border_RB", nullptr },
  { "Color.Button2_Active.Border_RB2", nullptr },
  { "Color.Button2_Normal.Border_LT", nullptr },
  { "Color.Button2_Normal.Border_LT2", nullptr },
  { "Color.Button2_Normal.Border_RB", nullptr },
  { "Color.Button2_Normal.Border_RB2", nullptr },
  { "Color.Button_Active.Border_LT", nullptr },
  { "Color.Button_Active.Border_LT2", nullptr },
  { "Color.Button_Active.Border_RB", nullptr },
  { "Color.Button_Active.Border_RB2", nullptr },
  { "Color.Button_Active.Text", nullptr },
  { "Color.Button_Normal.Border_LT", nullptr },
  { "Color.Button_Normal.Border_LT2", nullptr },
  { "Color.Button_Normal.Border_RB", nullptr },
  { "Color.Button_Normal.Border_RB2", nullptr },
  { "Color.ThinButton2_Normal.Border_LT", nullptr },
  { "Color.ThinButton2_Normal.Border_RB", nullptr },
};

std::vector<Order> strict_CheckBoxControl =
{
  { "Color.CheckBoxFill_Disabled", nullptr },
  { "Color.CheckBoxFill_Enabled", nullptr },
};

std::vector<Order> strict_Constants =
{
  { "Color.Black", nullptr },
  { "Color.Blue", nullptr },
  { "Color.Blue_Dark", nullptr },
  { "Color.Blue_Light", nullptr },
  { "Color.Blue_MediumDark", nullptr },
  { "Color.Blue_MediumLight", nullptr },
  { "Color.Blue_VeryDark", nullptr },
  { "Color.Blue_VeryLight", nullptr },
  { "Color.Brown", nullptr },
  { "Color.Brown_Dark", nullptr },
  { "Color.Brown_Light", nullptr },
  { "Color.Brown_MediumDark", nullptr },
  { "Color.Brown_MediumLight", nullptr },
  { "Color.Brown_VeryDark", nullptr },
  { "Color.Brown_VeryLight", nullptr },
  { "Color.Cyan", nullptr },
  { "Color.Cyan_Dark", nullptr },
  { "Color.Cyan_Light", nullptr },
  { "Color.Cyan_MediumDark", nullptr },
  { "Color.Cyan_MediumLight", nullptr },
  { "Color.Cyan_VeryDark", nullptr },
  { "Color.Cyan_VeryLight", nullptr },
  { "Color.Gray", nullptr },
  { "Color.Gray_Dark", nullptr },
  { "Color.Gray_Light", nullptr },
  { "Color.Gray_MediumDark", nullptr },
  { "Color.Gray_MediumLight", nullptr },
  { "Color.Gray_VeryDark", nullptr },
  { "Color.Gray_VeryLight", nullptr },
  { "Color.Gray_VeryVeryLight", nullptr },
  { "Color.Green", nullptr },
  { "Color.Green_Dark", nullptr },
  { "Color.Green_Light", nullptr },
  { "Color.Green_MediumDark", nullptr },
  { "Color.Green_MediumLight", nullptr },
  { "Color.Green_VeryDark", nullptr },
  { "Color.Green_VeryLight", nullptr },
  { "Color.Magenta", nullptr },
  { "Color.Magenta_Dark", nullptr },
  { "Color.Magenta_Light", nullptr },
  { "Color.Magenta_MediumDark", nullptr },
  { "Color.Magenta_MediumLight", nullptr },
  { "Color.Magenta_VeryDark", nullptr },
  { "Color.Magenta_VeryLight", nullptr },
  { "Color.None", nullptr },
  { "Color.Orange", nullptr },
  { "Color.Orange_Dark", nullptr },
  { "Color.Orange_Light", nullptr },
  { "Color.Orange_MediumDark", nullptr },
  { "Color.Orange_MediumLight", nullptr },
  { "Color.Orange_VeryDark", nullptr },
  { "Color.Orange_VeryLight", nullptr },
  { "Color.Pink", nullptr },
  { "Color.Pink_Dark", nullptr },
  { "Color.Pink_Light", nullptr },
  { "Color.Pink_MediumDark", nullptr },
  { "Color.Pink_MediumLight", nullptr },
  { "Color.Pink_VeryDark", nullptr },
  { "Color.Pink_VeryLight", nullptr },
  { "Color.Red", nullptr },
  { "Color.Red_Dark", nullptr },
  { "Color.Red_Light", nullptr },
  { "Color.Red_MediumDark", nullptr },
  { "Color.Red_MediumLight", nullptr },
  { "Color.Red_VeryDark", nullptr },
  { "Color.Red_VeryLight", nullptr },
  { "Color.White", nullptr },
  { "Color.Yellow", nullptr },
  { "Color.Yellow_Dark", nullptr },
  { "Color.Yellow_Light", nullptr },
  { "Color.Yellow_MediumDark", nullptr },
  { "Color.Yellow_MediumLight", nullptr },
  { "Color.Yellow_VeryDark", nullptr },
  { "Color.Yellow_VeryLight", nullptr },
};

std::vector<Order> strict_Custom =
{
  { "Color.Blue_VeryVeryLight", nullptr },
  { "Color.Custom01", nullptr },
  { "Color.Custom02", nullptr },
  { "Color.Custom03", nullptr },
  { "Color.Custom04", nullptr },
  { "Color.Custom05", nullptr },
  { "Color.Custom06", nullptr },
  { "Color.Custom07", nullptr },
  { "Color.Custom08", nullptr },
  { "Color.Custom09", nullptr },
  { "Color.Cyan_ExtremelyLight", nullptr },
  { "Color.Cyan_VeryVeryLight", nullptr },
  { "Color.Orange_VeryVeryLight", nullptr },
  { "Color.Yellow_ExtremelyLight", nullptr },
  { "Color.Yellow_VeryVeryLight", nullptr },
};

std::vector<Order> strict_EdgeTools =
{
  { "Color.ClosestSnap", nullptr },
  { "Color.EdgeCutter_TargetEdges", nullptr },
  { "Color.NotClosestSnap", nullptr },
};

std::vector<Order> strict_EditMeshControl =
{
  { "Color.EditMeshControl_Border", nullptr },
  { "Color.EditMeshControl_Selection", nullptr },
  { "Color.EditMeshControl_TileA", nullptr },
  { "Color.EditMeshControl_TileB", nullptr },
  { "Color.EditMeshControl_TileC", nullptr },
  { "Color.EditMeshControl_TileD", nullptr },
};

std::vector<Order> strict_EditorGrid =
{
  { "Color.AccentLines", nullptr },
  { "Color.CenterLines", nullptr },
  { "Color.GridAxisLetter", nullptr },
  { "Color.NormalLines", nullptr },
};

std::vector<Order> strict_ExtenderNode =
{
  { "Color.Extender_Back", nullptr },
};

std::vector<Order> strict_FontEditor =
{
  { "Color.FontCharactersBaseLine", nullptr },
  { "Color.FontCharactersBoundingBox", nullptr },
  { "Color.FontTextureBoundingBox", nullptr },
};

std::vector<Order> strict_GraphControl =
{
  { "GraphControl.AxesColor", nullptr },
  { "GraphControl.BackgroundColor", nullptr },
  { "GraphControl.ControlPointColor", nullptr },
  { "GraphControl.GridColor", nullptr },
  { "GraphControl.Inactive_AxesColor", nullptr },
  { "GraphControl.Inactive_TextColor", nullptr },
  { "GraphControl.SelectedControlPointColor", nullptr },
  { "GraphControl.SelectionBorderColor", nullptr },
  { "GraphControl.SelectionFillColor", nullptr },
  { "GraphControl.TextColor", nullptr },
};

std::vector<Order> strict_InfoTip =
{
  { "Color.InfoTip_Back", nullptr },
};

std::vector<Order> strict_InstancingTool =
{
  { "Color.InstancePreviewBackground", nullptr },
  { "Color.SelectedCount", nullptr },
  { "Color.SelectedExternalInstance", nullptr },
  { "Color.SelectedInstanceActiveGroup", nullptr },
  { "Color.SelectedInstanceInactiveGroup", nullptr },
  { "Color.UnselectedExternalInstance", nullptr },
};

std::vector<Order> strict_LayerItemControl =
{
  { "Color.LayerItem_Empty", nullptr },
  { "Color.LayerItem_Filled", nullptr },
  { "Color.LayerItem_Visible", nullptr },
  { "Color.LayerItemSeparator", nullptr },
};

std::vector<Order> strict_ListControl =
{
  { "ListControl.BackColor1", nullptr },
  { "ListControl.BackColor2", nullptr },
  { "ListControl.FocusedBackColor", nullptr },
  { "ListControl.NoInputFocusedBackColor", nullptr },
  { "ListControl.NoInputSelectedAndFocusedBackColor", nullptr },
  { "ListControl.NoInputSelectedBackColor", nullptr },
  { "ListControl.NoInputStringItemColor", nullptr },
  { "ListControl.NoInputStringItemSelectedColor", nullptr },
  { "ListControl.SelectedAndFocusedBackColor", nullptr },
  { "ListControl.SelectedBackColor", nullptr },
  { "ListControl.SelectedColumn", nullptr },
  { "ListControl.StringItemColor", nullptr },
  { "ListControl.StringItemSelectedColor", nullptr },
};

std::vector<Order> strict_ListControl2 =
{
  { "Color.ListControl.BackColor", nullptr },
  { "Color.ListControl.EmptyList_Text", nullptr },
  { "Color.ListControl.ItemBackColor_Even", nullptr },
  { "Color.ListControl.ItemBackColor_Hightlight", nullptr },
  { "Color.ListControl.ItemBackColor_Odd", nullptr },
  { "Color.ListControl.ItemBackColor_Selected", nullptr },
  { "Color.ListControl.ItemBackColor_SelectedNF", nullptr },
};

std::vector<Order> strict_MeshRenderer =
{
  { "Color.MeshRenderer_BoundingBox", nullptr },
  { "Color.MeshRenderer_Mapping_Fill", nullptr },
  { "Color.MeshRenderer_Mapping_Frame", nullptr },
  { "Color.MeshRenderer_Mapping_Points", nullptr },
  { "Color.MeshRenderer_Mapping_Wireframe", nullptr },
  { "Color.MeshRenderer_Normals", nullptr },
  { "Color.MeshRenderer_Points", nullptr },
  { "Color.MeshRenderer_Polygons_Flat", nullptr },
  { "Color.MeshRenderer_Polygons_Shaded", nullptr },
  { "Color.MeshRenderer_Polygons_Shaded_Ambient", nullptr },
  { "Color.MeshRenderer_Selection_Edges", nullptr },
  { "Color.MeshRenderer_Selection_Edges_Back", nullptr },
  { "Color.MeshRenderer_Selection_Points", nullptr },
  { "Color.MeshRenderer_Selection_Points_Back", nullptr },
  { "Color.MeshRenderer_Selection_Polygons", nullptr },
  { "Color.MeshRenderer_Selection_Polygons_Back", nullptr },
  { "Color.MeshRenderer_Selection_Polygons_Fill", nullptr },
  { "Color.MeshRenderer_TangentSpace_Binormal", nullptr },
  { "Color.MeshRenderer_TangentSpace_Normal", nullptr },
  { "Color.MeshRenderer_TangentSpace_Tangent", nullptr },
  { "Color.MeshRenderer_Wireframe", nullptr },
  { "Color.MeshRenderer_Wireframe_Back", nullptr },
};

std::vector<Order> strict_Misc =
{
  { "Color.BasePanel.Border_0", nullptr },
  { "Color.BasePanel.Border_1", nullptr },
  { "Color.BasePanel.Border_2", nullptr },
  { "Color.BasePanel.Border_3", nullptr },
  { "Color.ColumnBar.Border_Ex", nullptr },
  { "Color.ColumnBar.Border_LT", nullptr },
  { "Color.ColumnBar.Border_RB", nullptr },
  { "Color.DocumentPanel.Back", nullptr },
  { "Color.DocumentPanel.Text", nullptr },
  { "Color.ExtenderNode.Border_LT", nullptr },
  { "Color.ExtenderNode.Border_RB", nullptr },
  { "Color.HoveredNodePreview.Border", nullptr },
  { "Color.HyperText.Text_Accent", nullptr },
  { "Color.HyperText.Text_Default", nullptr },
  { "Color.HyperText.Text_Link", nullptr },
  { "Color.HyperText.Text_Softlink", nullptr },
  { "Color.List.Border_Focused", nullptr },
  { "Color.List.Border_Normal", nullptr },
  { "Color.MenuPanel.Border_LT", nullptr },
  { "Color.MenuPanel.Border_LT2", nullptr },
  { "Color.MenuPanel.Border_RB", nullptr },
  { "Color.MenuPanel.Border_RB2", nullptr },
  { "Color.MenuSeparator.Border_LT", nullptr },
  { "Color.MenuSeparator.Border_RB", nullptr },
  { "Color.StartPage.Background", nullptr },
  { "Color.StatusBar.Border", nullptr },
  { "Color.TabItem.BarStripe_Dark", nullptr },
  { "Color.TabItem.BarStripe_Light", nullptr },
  { "Color.TabItem.Border_Dark", nullptr },
  { "Color.TabItem.Border_Light", nullptr },
  { "FilePanel.SelectedListBackColor", nullptr },
};

std::vector<Order> strict_ProgressBarControl =
{
  { "Color.ProgressBar", nullptr },
};

std::vector<Order> strict_SceneryTool =
{
  { "Color.SceneryTool_GrayOutModels", nullptr },
};

std::vector<Order> strict_ScrollBar =
{
  { "Color.ScrollBarButton", nullptr },
  { "Color.ScrollBarHolder", nullptr },
  { "Color.ScrollBarSlider", nullptr },
  { "Color.ScrollDecorator.Border_LT", nullptr },
  { "Color.ScrollDecorator.Border_LT2", nullptr },
  { "Color.ScrollDecorator.Border_RB", nullptr },
  { "Color.ScrollDecorator.Border_RB2", nullptr },
};

std::vector<Order> strict_Shared =
{
  { "Color.AccentChar_Normal", nullptr },
  { "Color.AccentChar_Selected", nullptr },
  { "Color.Back_Accent", nullptr },
  { "Color.Back_Accent_Selected", nullptr },
  { "Color.Back_Grayed", nullptr },
  { "Color.Back_Hoverted", nullptr },
  { "Color.Back_Inverted", nullptr },
  { "Color.Back_Menu", nullptr },
  { "Color.Back_Menu_Light", nullptr },
  { "Color.Back_Normal", nullptr },
  { "Color.Back_PushedButton", nullptr },
  { "Color.Back_PushedButton2", nullptr },
  { "Color.Back_Selected", nullptr },
  { "Color.Holder_Normal", nullptr },
  { "Color.Text_Disabled", nullptr },
  { "Color.Text_Hovered", nullptr },
  { "Color.Text_Inverted", nullptr },
  { "Color.Text_Normal", nullptr },
  { "Color.Text_Selected", nullptr },
  { "SelectedBorderColor", nullptr },
  { "ToolTipBackgroundColor", nullptr },
};

std::vector<Order> strict_SimpleItemList =
{
  { "Color.SimpleItemList_BackSelected", nullptr },
  { "Color.SimpleItemList_SelectedBorder", nullptr },
  { "Color.SimpleItemList_TextNormal", nullptr },
  { "Color.SimpleItemList_TextSelected", nullptr },
};

std::vector<Order> strict_SimTileLineControl =
{
  { "Color.SimTimeLine_FrameMark", nullptr },
  { "Color.SimTimeLine_OffsetFill", nullptr },
};

std::vector<Order> strict_SliderControl =
{
  { "Color.Slider.Active", nullptr },
  { "Color.Slider.Inactive", nullptr },
};

std::vector<Order> strict_SplitterNode =
{
  { "Color.Splitter", nullptr },
};

std::vector<Order> strict_StatusBar =
{
  { "Color.StatusLine_BackClosedGroup", nullptr },
  { "Color.StatusLine_BackEntity", nullptr },
  { "Color.StatusLine_BackGrid", nullptr },
  { "Color.StatusLine_BackMesh", nullptr },
  { "Color.StatusLine_BackOpenGroup", nullptr },
  { "Color.StatusLine_Error", nullptr },
  { "Color.StatusLine_Warning", nullptr },
};

std::vector<Order> strict_StatusBarDecorator =
{
  { "Color.StatusBar_Error", nullptr },
  { "Color.StatusBar_Info", nullptr },
  { "Color.StatusBar_Success", nullptr },
  { "Color.StatusBar_Warning", nullptr },
};

std::vector<Order> strict_TabBarControl =
{
  { "TabItem.BackColor", nullptr },
};

std::vector<Order> strict_TabItemControl =
{
  { "Color.TabItem.BackActiveColor", nullptr },
  { "Color.TabItem.BackInactiveColor", nullptr },
  { "Color.TabItem.TextActiveColor", nullptr },
  { "Color.TabItem.TextInactiveColor", nullptr },
};

std::vector<Order> strict_TextStyleSheetEditor =
{
  { "Color.TextStyleSheetCharactersBaseLine", nullptr },
  { "Color.TextStyleSheetCharactersBoundingBox", nullptr },
  { "Color.TextStyleSheetTextureBoundingBox", nullptr },
};

std::vector<Order> strict_TitleBar =
{
  { "Color.TitleBarBackColor01_ActiveDocked", nullptr },
  { "Color.TitleBarBackColor01_ActiveFloating", nullptr },
  { "Color.TitleBarBackColor01_InactiveDocked", nullptr },
  { "Color.TitleBarBackColor01_InactiveFloating", nullptr },
  { "Color.TitleBarBackColor02_ActiveDocked", nullptr },
  { "Color.TitleBarBackColor02_ActiveFloating", nullptr },
  { "Color.TitleBarBackColor02_InactiveDocked", nullptr },
  { "Color.TitleBarBackColor02_InactiveFloating", nullptr },
  { "Color.TitleBarDragHandle", nullptr },
  { "Color.TitleBarText_Active", nullptr },
  { "Color.TitleBarText_Inactive", nullptr },
};

std::vector<Order> strict_TreeGridControl =
{
  { "TreeGrid.BackColor", nullptr },
  { "TreeGrid.BackColorNoInput", nullptr },
  { "TreeGrid.FocusedAndSelectedBackColor", nullptr },
  { "TreeGrid.FocusedAndSelectedBackColorNoInput", nullptr },
  { "TreeGrid.FocusedAndSelectedGridColor", nullptr },
  { "TreeGrid.FocusedAndSelectedGridColorNoInput", nullptr },
  { "TreeGrid.FocusedBackColor", nullptr },
  { "TreeGrid.FocusedBackColorNoInput", nullptr },
  { "TreeGrid.FocusedGridColor", nullptr },
  { "TreeGrid.FocusedGridColorNoInput", nullptr },
  { "TreeGrid.GridColor", nullptr },
  { "TreeGrid.GridColorNoInput", nullptr },
  { "TreeGrid.Highlight.BackColor", nullptr },
  { "TreeGrid.Highlight.FocusedAndSelectedBackColor", nullptr },
  { "TreeGrid.Highlight.FocusedBackColor", nullptr },
  { "TreeGrid.Highlight.SelectedBackColor", nullptr },
  { "TreeGrid.Inactive.BackColor", nullptr },
  { "TreeGrid.Inactive.FocusedAndSelectedBackColor", nullptr },
  { "TreeGrid.Inactive.FocusedBackColor", nullptr },
  { "TreeGrid.Inactive.SelectedBackColor", nullptr },
  { "TreeGrid.SelectedBackColor", nullptr },
  { "TreeGrid.SelectedBackColorNoInput", nullptr },
  { "TreeGrid.SelectedGridColor", nullptr },
  { "TreeGrid.SelectedGridColorNoInput", nullptr },
  { "TreeGrid.SelectedTextColor", nullptr },
  { "TreeGrid.TextColor", nullptr },
};

std::vector<Order> strict_ViewControl =
{
  { "View.BackColor", nullptr },
};

std::vector<Order> strict_VisualBugsSystemExplorer =
{
  { "Color.Type_Bug", nullptr },
  { "Color.Type_Duplicate", nullptr },
  { "Color.Type_Note", nullptr },
  { "Color.Type_ToDo", nullptr },
};

std::vector<Order> strict_WorldRenderer =
{
  { "Color.WorldRenderer_EntityNames", nullptr },
  { "Color.WorldRenderer_FillSelection", nullptr },
  { "Color.WorldRenderer_FillSelection_Locked", nullptr },
  { "Color.WorldRenderer_SelectedEntityNames", nullptr },
};

//! Objects in Color group
std::vector<Order> strict_colors_group =
{
  { "ColorGroup.AnimTimeLineControl", &strict_AnimTimeLineControl },
  { "ColorGroup.AnimTrackEditorControl", &strict_AnimTrackEditorControl },
  { "ColorGroup.AssetTrackingSystemExplorer", &strict_AssetTrackingSystemExplorer },
  { "ColorGroup.AxisPreview", &strict_AxisPreview },
  { "ColorGroup.Borders", &strict_Borders },
  { "ColorGroup.Buttons", &strict_Buttons },
  { "ColorGroup.CheckBoxControl", &strict_CheckBoxControl },
  { "ColorGroup.Constants", &strict_Constants },
  { "ColorGroup.Custom", &strict_Custom },
  { "ColorGroup.EdgeTools", &strict_EdgeTools },
  { "ColorGroup.EditMeshControl", &strict_EditMeshControl },
  { "ColorGroup.EditorGrid", &strict_EditorGrid },
  { "ColorGroup.ExtenderNode", &strict_ExtenderNode },
  { "ColorGroup.FontEditor", &strict_FontEditor },
  { "ColorGroup.GraphControl", &strict_GraphControl },
  { "ColorGroup.InfoTip", &strict_InfoTip },
  { "ColorGroup.InstancingTool", &strict_InstancingTool },
  { "ColorGroup.LayerItemControl", &strict_LayerItemControl },
  { "ColorGroup.ListControl", &strict_ListControl },
  { "ColorGroup.ListControl2", &strict_ListControl2 },
  { "ColorGroup.MeshRenderer", &strict_MeshRenderer },
  { "ColorGroup.Misc", &strict_Misc },
  { "ColorGroup.ProgressBarControl", &strict_ProgressBarControl },
  { "ColorGroup.SceneryTool", &strict_SceneryTool },
  { "ColorGroup.ScrollBar", &strict_ScrollBar },
  { "ColorGroup.Shared", &strict_Shared },
  { "ColorGroup.SimpleItemList", &strict_SimpleItemList },
  { "ColorGroup.SimTileLineControl", &strict_SimTileLineControl },
  { "ColorGroup.SliderControl", &strict_SliderControl },
  { "ColorGroup.SplitterNode", &strict_SplitterNode },
  { "ColorGroup.StatusBar", &strict_StatusBar },
  { "ColorGroup.StatusBarDecorator", &strict_StatusBarDecorator },
  { "ColorGroup.TabBarControl", &strict_TabBarControl },
  { "ColorGroup.TabItemControl", &strict_TabItemControl },
  { "ColorGroup.TextStyleSheetEditor", &strict_TextStyleSheetEditor },
  { "ColorGroup.TitleBar", &strict_TitleBar },
  { "ColorGroup.TreeGridControl", &strict_TreeGridControl },
  { "ColorGroup.ViewControl", &strict_ViewControl },
  { "ColorGroup.VisualBugsSystemExplorer", &strict_VisualBugsSystemExplorer },
  { "ColorGroup.WorldRenderer", &strict_WorldRenderer },
};

//! Fields of Font group
std::vector<Order> strict_font_group =
{
  { "HighlightAcclr", nullptr },
  { "UnderlineAcclr", nullptr },
  { "LargeFonts", nullptr },
  { "FontSize", nullptr },
};

//! Fields of Icons group
std::vector<Order> strict_icons_group =
{
  { "UseDarkThemeIcons", nullptr },
};

//! Fields of Font object
std::vector<Order> strict_Font =
{
  { "Bold", nullptr },
  { "Italic", nullptr },
  { "Ink", nullptr },
  { "Paper", nullptr },
  { "Font", nullptr },
};

//! Object in Forse Font field
std::vector<Order> strict_Force_Font =
{
  { "Common text", &strict_Font },
};

//! Objects in Scripting group
std::vector<Order> strict_script_group =
{
  { "Force font", nullptr },
  { "Common text", &strict_Font },
  { "Comment", &strict_Font },
  { "Keyword", &strict_Font },
  { "Expression", &strict_Font },
  { "Operator", &strict_Font },
  { "Bracket", &strict_Font },
  { "Number", &strict_Font },
  { "String", &strict_Font },
  { "Valid variable", &strict_Font },
  { "Invalid variable", &strict_Font },
  { "Function", &strict_Font },
  { "Macro program", &strict_Font },
  { "Event", &strict_Font },
  { "This hint", &strict_Font },
  { "Object", &strict_Font },
  { "Error", &strict_Font },
  { "Selected valid variable", &strict_Font },
  { "Disabled text", &strict_Font },
  { "Hinted variable", &strict_Font },
  { "Hint comment", &strict_Font },
};

//! Fields of Text group
std::vector<Order> strict_Text_group =
{
  { "NormalTextColor", nullptr },
  { "SelectedTextColor", nullptr },
  { "NormalBackgroundColor", nullptr },
  { "SelectedTextBackgroundColor", nullptr },
  { "SimilarTextBackgroundColor", nullptr },
  { "LineNumberTextColor", nullptr },
  { "LineNumberBackColor", nullptr },
};

std::set<std::string> globalGroups = {
  "Colors",
  "Fonts",
  "Icons",
  "ScriptSourceColorizing",
  "TextControl"
};

//! All Groups
std::vector<Order> strict_groups =
{
  { "Colors", &strict_colors_group },
  { "Fonts", &strict_font_group },
  { "Icons", &strict_icons_group },
  { "ScriptSourceColorizing", &strict_script_group },
  { "TextControl", &strict_Text_group }
};

//! Write Holder in strict order
void write_strict(
  std::ofstream& of,
  std::weak_ptr<Holder> link_holder,
  std::vector<Order>* orders,
  uint32_t depth)
{
  if (orders)
  {
    auto holder = link_holder.lock();
    for (auto& id : *orders)
    {
      if (auto link = holder->find(id.first); !link.expired())
      {
        if (auto pair = std::dynamic_pointer_cast<Pair>(link.lock()); pair)
        {
          if (auto link_object = pair->getObject(); !link_object.expired())
          {
            auto object = link_object.lock();
            std::vector<Order>* objectOrder = nullptr;
            if (!id.second->empty())
            {
              objectOrder = id.second->at(0).second;
            }

            pair->write_begin(of, depth);
            object->write_begin(of, depth);
            write_strict(of, object, objectOrder, depth + 1);
            object->write_end(of, depth);
            of << '\n';
            pair->write_end(of, depth);
          } else {
            pair->write(of, depth);
            if (id.first != "Force font") // smell
            {
              pair->write_end(of, depth);
            } else {
              of << '\n';
            }
          }
          continue;
        }
        if (auto child = std::dynamic_pointer_cast<Holder>(link.lock()); child)
        {
          child->write_begin(of, depth);
          write_strict(of, child, id.second, depth + 1);
          child->write_end(of, depth);
          if (globalGroups.count(id.first))
          {
            of << ";\n";
          } else {
            of << "\n";
          }
          continue;
        }
      } else {
        // If not found
        auto i = depth;
        while (i > 0)
        {
          of << "  ";
          i--;
        }
        of << id.first << " {};\n";
      }
    }
  }
}

void colormalfu::write_schema_strict(std::ofstream& of, std::weak_ptr<Schema> link_schema)
{
  if (link_schema.expired())
  {
    // TODO: log err
    return;
  }
  write_strict(of, link_schema, &strict_groups, 0);
}