﻿#include "pch.h"

#include "../generated/sethemeParser.h"
#include "../generated/sethemeLexer.h"
#include "../SchemaTreeListener.h"

#pragma comment(lib, "../../Release/core.lib")
#pragma comment(lib, "../../Release/antlr4-runtime.lib")

#include <fstream>

NAMESPACE_TEST(Antlr, Antlr, Default)
{
  int i = 0;
  antlrcpp::Any test(i);
  EXPECT_EQ(0, test.as<int>());
}

NAMESPACE_TEST(Antlr, Antlr, Tree_from_G4)
{
  std::string p = "D:/Repose/colorful-malfunction/DefaultTheme.setheme";
  std::ifstream stream (p);
  EXPECT_TRUE(stream.is_open());

  antlr4::ANTLRInputStream input(stream);
  sethemeLexer lexer(&input);

  antlr4::CommonTokenStream tokens(&lexer);
  sethemeParser parser(&tokens);

  antlr4::tree::ParseTree* tree = parser.setheme();

  auto res = tree->toStringTree(&parser);

  EXPECT_NE("", res);
  std::ofstream o("test_out.txt");
  o << res;
  o.close();
}
NAMESPACE_TEST(Antlr, Antlr, List_from_G4)
{
  std::string p = "D:/Repose/colorful-malfunction/DefaultTheme.setheme";
  std::ifstream stream(p);
  if (stream.is_open())
  {
    antlr4::ANTLRInputStream input(stream);
    sethemeLexer lexer(&input);

    antlr4::CommonTokenStream tokens(&lexer);
    sethemeParser parser(&tokens);

    antlr4::tree::ParseTree* tree = parser.setheme();
    SchemaTreeListener listener;
    antlr4::tree::ParseTreeWalker::DEFAULT.walk(&listener, tree);

    std::ofstream of("BattleTest.txt", std::ios::binary);
    auto resultSchema = listener.getSchema().lock();
    resultSchema->write(of);
  }
}