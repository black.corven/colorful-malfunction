#include "pch.h"

#include "../Schema.h"

#include "../pair.h"

#pragma comment(lib, "../../Release/core.lib")

#include <fstream>

using namespace colormalfu;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Schema
NAMESPACE_TEST(Schema, Schema, Write)
{
  Schema file;
  auto group1 = std::make_shared<Group>("Group1");
  file.insert(group1);

  auto obj = std::make_shared<Object>("Object");
  group1->insert(obj);
  group1->insert(std::make_shared<Pair>("Pair String", Pair::ValueType::STRING, "defined"));
  group1->insert(std::make_shared<Pair>("Pair Object", std::make_shared<Object>("ObjectNested")));

  obj->insert(std::make_shared<Pair>("Pair int", Pair::ValueType::NUMBER, 123));
  obj->insert(std::make_shared<Pair>("Pair hex", Pair::ValueType::HEX, 0xFF000000));
  obj->insert(std::make_shared<Pair>("Pair literal", Pair::ValueType::LITERAL, "true"));

  ASSERT_TRUE(!obj->find("Pair hex").expired());

  std::ofstream of("test_out2.txt", std::ios::binary);
  file.write(of);
}


NAMESPACE_TEST(Schema, Schema, Read)
{
  auto schema = Schema::read("WRONG");
  EXPECT_EQ(nullptr, schema);

  schema = Schema::read("../core/core-test/Resource/test_input.txt");
  ASSERT_NE(nullptr, schema);

  auto group_link = schema->findHolder("Group1");
  ASSERT_FALSE(group_link.expired());

  {
    auto pair_link = group_link.lock()->find("Pair Empty");
    ASSERT_FALSE(pair_link.expired());
  }

  {
    auto object_link = group_link.lock()->find("Object");
    ASSERT_FALSE(object_link.expired());
    auto object = std::dynamic_pointer_cast<Holder>(object_link.lock());
    ASSERT_TRUE(object);
    EXPECT_TRUE(object->find("Pair").expired());
    auto pair_link = object->find("Pair int");
    ASSERT_FALSE(pair_link.expired());
    auto pair = std::dynamic_pointer_cast<Pair>(pair_link.lock());
    ASSERT_TRUE(pair);
    EXPECT_EQ(Pair::ValueType::NUMBER, pair->getType());
    EXPECT_EQ("123", pair->getValueString());
  }
}

NAMESPACE_TEST(Schema, Schema, Write_Strict)
{
  auto schema = Schema::read("../core/core-test/Resource/Default.setheme");
  ASSERT_NE(nullptr, schema);

  std::ofstream of("Default_Strict_Export.setheme", std::ios::binary);
  write_schema_strict(of, schema);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Group 

NAMESPACE_TEST(Schema, Group, Find)
{
  auto group = std::make_shared<Group>("Group");
  group->insert(std::make_shared<Pair>("Pair", Pair::ValueType::NUMBER, 1));
  group->insert(std::make_shared<Object>("Object"));

  {
    auto res = group->find("Undefined");
    EXPECT_TRUE(res.expired());
  }

  {
    auto res = group->find("Pair");
    ASSERT_FALSE(res.expired());
    auto var = std::dynamic_pointer_cast<Pair>(res.lock());
    EXPECT_NE(nullptr, var);
    EXPECT_EQ("1", var->getValueString());
  }

  {
    auto res = group->find("Object");
    ASSERT_FALSE(res.expired());
    EXPECT_EQ(nullptr, std::dynamic_pointer_cast<Pair>(res.lock()));
    auto var = std::dynamic_pointer_cast<Object>(res.lock());
    EXPECT_NE(nullptr, var);
  }
}

NAMESPACE_TEST(Schema, Group, FindPair)
{
  auto group = std::make_shared<Group>("Group");
  group->insert(std::make_shared<Pair>("Pair", Pair::ValueType::NUMBER, 1));
  group->insert(std::make_shared<Object>("Object"));

  {
    auto res = group->findPair("Undefined");
    EXPECT_TRUE(res.expired());
  }

  {
    auto res = group->findPair("Object");
    EXPECT_TRUE(res.expired());
  }

  {
    auto res = group->findPair("Pair");
    ASSERT_FALSE(res.expired());
    EXPECT_EQ("1", res.lock()->getValueString());
  }
}

NAMESPACE_TEST(Schema, Group, FindObject)
{
  auto group = std::make_shared<Group>("Group");
  group->insert(std::make_shared<Pair>("Pair", Pair::ValueType::NUMBER, 1));
  group->insert(std::make_shared<Object>("Object"));
  
  {
    auto res = group->findObject("Undefined");
    EXPECT_TRUE(res.expired());
  }

  {
    auto res = group->findObject("Pair");
    EXPECT_TRUE(res.expired());
  }

  {
    auto res = group->findObject("Object");
    EXPECT_FALSE(res.expired());
    EXPECT_EQ("Object", res.lock()->getIdentifier());
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Object
NAMESPACE_TEST(Schema, Object, Create)
{
  auto obj = std::make_shared<Object>("Object");
  EXPECT_EQ("Object", obj->getIdentifier());
}

NAMESPACE_TEST(Schema, Object, InsertAndFindPair)
{
  auto obj = std::make_shared<Object>("Object");
  auto pair = std::make_shared<Pair>("Pair Weak", Pair::ValueType::NUMBER, 1);
  obj->insert(pair);
  obj->insert(std::make_shared<Pair>("Pair Move", Pair::ValueType::NUMBER, 2));
  EXPECT_EQ("1", obj->findPair("Pair Weak").lock()->getValueString());
  EXPECT_EQ("2", obj->findPair("Pair Move").lock()->getValueString());
}

NAMESPACE_TEST(Schema, Object, InsertSamePair)
{
  auto obj = std::make_shared<Object>("Object");
  auto p = std::make_shared<Pair>("Pair Move", Pair::ValueType::NUMBER, 1);
  obj->insert(p);
  EXPECT_EQ("1", obj->findPair("Pair Move").lock()->getValueString());
  *p = Pair("Pair Move", Pair::ValueType::IDENTIFIER, "2");
  EXPECT_EQ("2", obj->findPair("Pair Move").lock()->getValueString());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BaseProperty

NAMESPACE_TEST(Schema, BaseProperty, Create)
{
  BaseProperty p("Property");
  EXPECT_EQ("Property", p.getIdentifier());
}

NAMESPACE_TEST(Schema, BaseProperty, LessThen)
{
  BaseProperty p1("Property");
  BaseProperty p2("LessProperty");
  EXPECT_LT(p2, p1);
}

NAMESPACE_TEST(Schema, BaseProperty, Comparator)
{
  std::set<std::shared_ptr<BaseProperty>, ComparatorProperty_ptr> container;
  container.insert(std::make_shared<BaseProperty>("Second"));
  container.insert(std::make_shared<BaseProperty>("First"));
  container.insert(std::make_shared<BaseProperty>("First"));
  auto it = container.begin();

  ASSERT_EQ(2, container.size());
  EXPECT_EQ("First", (*it++)->getIdentifier());
  EXPECT_EQ("Second", (*it)->getIdentifier());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pair
NAMESPACE_TEST(Schema, Pair, Create_object_pair)
{
  Pair pair("Pair Object", std::make_shared<Object>("NestedObject"));
  EXPECT_EQ(Pair::ValueType::OBJECT, pair.getType());
  EXPECT_EQ("Pair Object", pair.getIdentifier());
  EXPECT_EQ("NestedObject", pair.getValueString());
  //ASSERT_EQ("NestedObject", pair.getObject());
}

NAMESPACE_TEST(Schema, Pair, Create_string_pair)
{
  Pair pair("Pair String", Pair::ValueType::STRING, "defined");
  EXPECT_EQ(Pair::ValueType::STRING, pair.getType());
  EXPECT_EQ("Pair String", pair.getIdentifier());
  EXPECT_EQ("defined", pair.getValueString());
}

NAMESPACE_TEST(Schema, Pair, Change_string_pair)
{
  Pair pair("Pair String", Pair::ValueType::STRING, "defined");
  pair.setValue("Any string");
  EXPECT_EQ("Any string", pair.getValueString());
  pair.setValue("true");
  EXPECT_EQ("true", pair.getValueString());
}

NAMESPACE_TEST(Schema, Pair, LessThan)
{
  Pair pair1("Font", Pair::ValueType::STRING, "defined");
  Pair pair2("Color", Pair::ValueType::STRING, "defined");
  EXPECT_LT(pair2, pair1);
  EXPECT_FALSE(pair1 < pair2);
}

NAMESPACE_TEST(Schema, Pair, Create_number_pair)
{
  Pair pair("Pair int", Pair::ValueType::NUMBER, 123);
  EXPECT_EQ(Pair::ValueType::NUMBER, pair.getType());
  EXPECT_EQ("123", pair.getValueString());
}

NAMESPACE_TEST(Schema, Pair, Change_number_pair)
{
  Pair pair("Pair hex", Pair::ValueType::NUMBER, 123);
  pair.setValue("qwe");
  EXPECT_EQ("123", pair.getValueString());
  pair.setValue("400");
  EXPECT_EQ("400", pair.getValueString());
}

NAMESPACE_TEST(Schema, Pair, Create_hex_pair)
{
  Pair pair("Pair hex", Pair::ValueType::HEX, 0xFF000000);
  EXPECT_EQ(Pair::ValueType::HEX, pair.getType());
  EXPECT_EQ("FF000000", pair.getValueString());
}

NAMESPACE_TEST(Schema, Pair, Change_hex_pair)
{
  Pair pair("Pair hex", Pair::ValueType::HEX, 0xFF000000);
  pair.setValue("qwe");
  EXPECT_EQ("FF000000", pair.getValueString());
  pair.setValue("abc");
  EXPECT_EQ("00000ABC", pair.getValueString());
  pair.setValue("000F000F");
  EXPECT_EQ("000F000F", pair.getValueString());
}

NAMESPACE_TEST(Schema, Pair, Create_literal_pair)
{
  Pair pair("Pair literal", Pair::ValueType::LITERAL, "true");
  EXPECT_EQ("true", pair.getValueString());

  Pair pair1("Pair literal", Pair::ValueType::LITERAL, "True");
  EXPECT_EQ("True", pair1.getValueString());

  Pair pair2("Pair literal", Pair::ValueType::LITERAL, "TRUE");
  EXPECT_EQ("TRUE", pair2.getValueString());
}

NAMESPACE_TEST(Schema, Pair, Change_literal_pair)
{
  Pair pair("Pair literal", Pair::ValueType::LITERAL, "true");
  pair.setValue("Any string");
  EXPECT_EQ("true", pair.getValueString());
  pair.setValue("True");
  EXPECT_EQ("True", pair.getValueString());
  pair.setValue("FALSE");
  EXPECT_EQ("FALSE", pair.getValueString());
}

