#include "pch.h"

#include "Schema.h"
#include "pair.h"
#include "object.h"

#include "generated/sethemeLexer.h"
#include "generated/sethemeParser.h"
#include "SchemaTreeListener.h"

using namespace colormalfu;

std::shared_ptr<Schema> Schema::read(std::string filePath)
{
  std::ifstream stream(filePath);
  if (stream.is_open())
  {
    antlr4::ANTLRInputStream input(stream);
    sethemeLexer lexer(&input);

    antlr4::CommonTokenStream tokens(&lexer);
    sethemeParser parser(&tokens);

    antlr4::tree::ParseTree* tree = parser.setheme();
    SchemaTreeListener listener;
    antlr4::tree::ParseTreeWalker::DEFAULT.walk(&listener, tree);
    if (auto link = listener.getSchema(); !link.expired())
    {
      auto p = link.lock();
      p->setPath(filePath);
      return p;
    }
  }
  // TODO: Error here?
  return nullptr;
}

void Schema::write(fs::path outputFile)
{
  //! cos LF used as end of line
  std::ofstream of(outputFile, std::ios::binary);
  if(of.is_open())
  {
    write(of);
    of.close();
  }
}

void Schema::write(std::ofstream& of)
{
  for (auto& property : m_data)
  {
    property->write(of);
    of << ";\n";
  }
}
