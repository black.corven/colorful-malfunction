
// Generated from setheme.g4 by ANTLR 4.9

#include "pch.h"

#include "sethemeListener.h"

#include "sethemeParser.h"


using namespace antlrcpp;
using namespace antlr4;

sethemeParser::sethemeParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

sethemeParser::~sethemeParser() {
  delete _interpreter;
}

std::string sethemeParser::getGrammarFileName() const {
  return "setheme.g4";
}

const std::vector<std::string>& sethemeParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& sethemeParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- SethemeContext ------------------------------------------------------------------

sethemeParser::SethemeContext::SethemeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* sethemeParser::SethemeContext::EOF() {
  return getToken(sethemeParser::EOF, 0);
}

std::vector<sethemeParser::GroupContext *> sethemeParser::SethemeContext::group() {
  return getRuleContexts<sethemeParser::GroupContext>();
}

sethemeParser::GroupContext* sethemeParser::SethemeContext::group(size_t i) {
  return getRuleContext<sethemeParser::GroupContext>(i);
}

std::vector<sethemeParser::ObjContext *> sethemeParser::SethemeContext::obj() {
  return getRuleContexts<sethemeParser::ObjContext>();
}

sethemeParser::ObjContext* sethemeParser::SethemeContext::obj(size_t i) {
  return getRuleContext<sethemeParser::ObjContext>(i);
}


size_t sethemeParser::SethemeContext::getRuleIndex() const {
  return sethemeParser::RuleSetheme;
}

void sethemeParser::SethemeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSetheme(this);
}

void sethemeParser::SethemeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSetheme(this);
}

sethemeParser::SethemeContext* sethemeParser::setheme() {
  SethemeContext *_localctx = _tracker.createInstance<SethemeContext>(_ctx, getState());
  enterRule(_localctx, 0, sethemeParser::RuleSetheme);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(29);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case sethemeParser::IDENTIFIER:
      case sethemeParser::IDENTIFIER_WS: {
        enterOuterAlt(_localctx, 1);
        setState(22); 
        _errHandler->sync(this);
        _la = _input->LA(1);
        do {
          setState(18);
          _errHandler->sync(this);
          switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx)) {
          case 1: {
            setState(16);
          group();
            break;
          }

          case 2: {
            setState(17);
            obj();
            break;
          }

          default:
            break;
          }
          setState(20);
          match(sethemeParser::T__0);
          setState(24); 
          _errHandler->sync(this);
          _la = _input->LA(1);
        } while (_la == sethemeParser::IDENTIFIER

        || _la == sethemeParser::IDENTIFIER_WS);
        setState(26);
        match(sethemeParser::EOF);
        break;
      }

      case sethemeParser::EOF: {
        enterOuterAlt(_localctx, 2);
        setState(28);
        match(sethemeParser::EOF);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- GroupContext ------------------------------------------------------------------

sethemeParser::GroupContext::GroupContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

sethemeParser::KeyContext* sethemeParser::GroupContext::key() {
  return getRuleContext<sethemeParser::KeyContext>(0);
}

std::vector<sethemeParser::ObjContext *> sethemeParser::GroupContext::obj() {
  return getRuleContexts<sethemeParser::ObjContext>();
}

sethemeParser::ObjContext* sethemeParser::GroupContext::obj(size_t i) {
  return getRuleContext<sethemeParser::ObjContext>(i);
}

std::vector<sethemeParser::PairContext *> sethemeParser::GroupContext::pair() {
  return getRuleContexts<sethemeParser::PairContext>();
}

sethemeParser::PairContext* sethemeParser::GroupContext::pair(size_t i) {
  return getRuleContext<sethemeParser::PairContext>(i);
}


size_t sethemeParser::GroupContext::getRuleIndex() const {
  return sethemeParser::RuleGroup;
}

void sethemeParser::GroupContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGroup(this);
}

void sethemeParser::GroupContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGroup(this);
}

sethemeParser::GroupContext* sethemeParser::group() {
  GroupContext *_localctx = _tracker.createInstance<GroupContext>(_ctx, getState());
  enterRule(_localctx, 2, sethemeParser::RuleGroup);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(45);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(31);
      key();
      setState(32);
      match(sethemeParser::T__1);
      setState(35); 
      _errHandler->sync(this);
      _la = _input->LA(1);
      do {
        setState(35);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
        case 1: {
          setState(33);
          obj();
          break;
        }

        case 2: {
          setState(34);
          pair();
          break;
        }

        default:
          break;
        }
        setState(37); 
        _errHandler->sync(this);
        _la = _input->LA(1);
      } while (_la == sethemeParser::IDENTIFIER

      || _la == sethemeParser::IDENTIFIER_WS);
      setState(39);
      match(sethemeParser::T__2);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(41);
      key();
      setState(42);
      match(sethemeParser::T__1);
      setState(43);
      match(sethemeParser::T__2);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjContext ------------------------------------------------------------------

sethemeParser::ObjContext::ObjContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

sethemeParser::KeyContext* sethemeParser::ObjContext::key() {
  return getRuleContext<sethemeParser::KeyContext>(0);
}

std::vector<sethemeParser::PairContext *> sethemeParser::ObjContext::pair() {
  return getRuleContexts<sethemeParser::PairContext>();
}

sethemeParser::PairContext* sethemeParser::ObjContext::pair(size_t i) {
  return getRuleContext<sethemeParser::PairContext>(i);
}


size_t sethemeParser::ObjContext::getRuleIndex() const {
  return sethemeParser::RuleObj;
}

void sethemeParser::ObjContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterObj(this);
}

void sethemeParser::ObjContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitObj(this);
}

sethemeParser::ObjContext* sethemeParser::obj() {
  ObjContext *_localctx = _tracker.createInstance<ObjContext>(_ctx, getState());
  enterRule(_localctx, 4, sethemeParser::RuleObj);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(62);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 7, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(47);
      key();
      setState(48);
      match(sethemeParser::T__1);
      setState(52); 
      _errHandler->sync(this);
      _la = _input->LA(1);
      do {
        setState(49);
        pair();
        setState(50);
        match(sethemeParser::T__0);
        setState(54); 
        _errHandler->sync(this);
        _la = _input->LA(1);
      } while (_la == sethemeParser::IDENTIFIER

      || _la == sethemeParser::IDENTIFIER_WS);
      setState(56);
      match(sethemeParser::T__2);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(58);
      key();
      setState(59);
      match(sethemeParser::T__1);
      setState(60);
      match(sethemeParser::T__2);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PairContext ------------------------------------------------------------------

sethemeParser::PairContext::PairContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

sethemeParser::KeyContext* sethemeParser::PairContext::key() {
  return getRuleContext<sethemeParser::KeyContext>(0);
}

sethemeParser::ValueContext* sethemeParser::PairContext::value() {
  return getRuleContext<sethemeParser::ValueContext>(0);
}


size_t sethemeParser::PairContext::getRuleIndex() const {
  return sethemeParser::RulePair;
}

void sethemeParser::PairContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPair(this);
}

void sethemeParser::PairContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPair(this);
}

sethemeParser::PairContext* sethemeParser::pair() {
  PairContext *_localctx = _tracker.createInstance<PairContext>(_ctx, getState());
  enterRule(_localctx, 6, sethemeParser::RulePair);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(71);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(64);
      key();
      setState(65);
      match(sethemeParser::T__3);
      setState(66);
      value();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(68);
      key();
      setState(69);
      match(sethemeParser::T__3);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- KeyContext ------------------------------------------------------------------

sethemeParser::KeyContext::KeyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* sethemeParser::KeyContext::IDENTIFIER() {
  return getToken(sethemeParser::IDENTIFIER, 0);
}

tree::TerminalNode* sethemeParser::KeyContext::IDENTIFIER_WS() {
  return getToken(sethemeParser::IDENTIFIER_WS, 0);
}

sethemeParser::TypedContext* sethemeParser::KeyContext::typed() {
  return getRuleContext<sethemeParser::TypedContext>(0);
}

sethemeParser::Typed_linkContext* sethemeParser::KeyContext::typed_link() {
  return getRuleContext<sethemeParser::Typed_linkContext>(0);
}


size_t sethemeParser::KeyContext::getRuleIndex() const {
  return sethemeParser::RuleKey;
}

void sethemeParser::KeyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterKey(this);
}

void sethemeParser::KeyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitKey(this);
}

sethemeParser::KeyContext* sethemeParser::key() {
  KeyContext *_localctx = _tracker.createInstance<KeyContext>(_ctx, getState());
  enterRule(_localctx, 8, sethemeParser::RuleKey);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(77);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(73);
      match(sethemeParser::IDENTIFIER);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(74);
      match(sethemeParser::IDENTIFIER_WS);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(75);
      typed();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(76);
      typed_link();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValueContext ------------------------------------------------------------------

sethemeParser::ValueContext::ValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* sethemeParser::ValueContext::STRING() {
  return getToken(sethemeParser::STRING, 0);
}

tree::TerminalNode* sethemeParser::ValueContext::DEC_NUMBER() {
  return getToken(sethemeParser::DEC_NUMBER, 0);
}

tree::TerminalNode* sethemeParser::ValueContext::HEX_NUMBER() {
  return getToken(sethemeParser::HEX_NUMBER, 0);
}

tree::TerminalNode* sethemeParser::ValueContext::LITERAL() {
  return getToken(sethemeParser::LITERAL, 0);
}

sethemeParser::TypedContext* sethemeParser::ValueContext::typed() {
  return getRuleContext<sethemeParser::TypedContext>(0);
}


size_t sethemeParser::ValueContext::getRuleIndex() const {
  return sethemeParser::RuleValue;
}

void sethemeParser::ValueContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterValue(this);
}

void sethemeParser::ValueContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitValue(this);
}

sethemeParser::ValueContext* sethemeParser::value() {
  ValueContext *_localctx = _tracker.createInstance<ValueContext>(_ctx, getState());
  enterRule(_localctx, 10, sethemeParser::RuleValue);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(84);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case sethemeParser::STRING: {
        enterOuterAlt(_localctx, 1);
        setState(79);
        match(sethemeParser::STRING);
        break;
      }

      case sethemeParser::DEC_NUMBER: {
        enterOuterAlt(_localctx, 2);
        setState(80);
        match(sethemeParser::DEC_NUMBER);
        break;
      }

      case sethemeParser::HEX_NUMBER: {
        enterOuterAlt(_localctx, 3);
        setState(81);
        match(sethemeParser::HEX_NUMBER);
        break;
      }

      case sethemeParser::LITERAL: {
        enterOuterAlt(_localctx, 4);
        setState(82);
        match(sethemeParser::LITERAL);
        break;
      }

      case sethemeParser::IDENTIFIER: {
        enterOuterAlt(_localctx, 5);
        setState(83);
        typed();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypedContext ------------------------------------------------------------------

sethemeParser::TypedContext::TypedContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> sethemeParser::TypedContext::IDENTIFIER() {
  return getTokens(sethemeParser::IDENTIFIER);
}

tree::TerminalNode* sethemeParser::TypedContext::IDENTIFIER(size_t i) {
  return getToken(sethemeParser::IDENTIFIER, i);
}


size_t sethemeParser::TypedContext::getRuleIndex() const {
  return sethemeParser::RuleTyped;
}

void sethemeParser::TypedContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTyped(this);
}

void sethemeParser::TypedContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTyped(this);
}

sethemeParser::TypedContext* sethemeParser::typed() {
  TypedContext *_localctx = _tracker.createInstance<TypedContext>(_ctx, getState());
  enterRule(_localctx, 12, sethemeParser::RuleTyped);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(86);
    match(sethemeParser::IDENTIFIER);
    setState(89); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(87);
      match(sethemeParser::T__4);
      setState(88);
      match(sethemeParser::IDENTIFIER);
      setState(91); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == sethemeParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Typed_linkContext ------------------------------------------------------------------

sethemeParser::Typed_linkContext::Typed_linkContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> sethemeParser::Typed_linkContext::IDENTIFIER() {
  return getTokens(sethemeParser::IDENTIFIER);
}

tree::TerminalNode* sethemeParser::Typed_linkContext::IDENTIFIER(size_t i) {
  return getToken(sethemeParser::IDENTIFIER, i);
}


size_t sethemeParser::Typed_linkContext::getRuleIndex() const {
  return sethemeParser::RuleTyped_link;
}

void sethemeParser::Typed_linkContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTyped_link(this);
}

void sethemeParser::Typed_linkContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<sethemeListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTyped_link(this);
}

sethemeParser::Typed_linkContext* sethemeParser::typed_link() {
  Typed_linkContext *_localctx = _tracker.createInstance<Typed_linkContext>(_ctx, getState());
  enterRule(_localctx, 14, sethemeParser::RuleTyped_link);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(93);
    match(sethemeParser::IDENTIFIER);
    setState(94);
    match(sethemeParser::T__4);
    setState(95);
    match(sethemeParser::IDENTIFIER);
    setState(96);
    match(sethemeParser::T__5);
    setState(97);
    match(sethemeParser::IDENTIFIER);
    setState(98);
    match(sethemeParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> sethemeParser::_decisionToDFA;
atn::PredictionContextCache sethemeParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN sethemeParser::_atn;
std::vector<uint16_t> sethemeParser::_serializedATN;

std::vector<std::string> sethemeParser::_ruleNames = {
  "setheme", "group", "obj", "pair", "key", "value", "typed", "typed_link"
};

std::vector<std::string> sethemeParser::_literalNames = {
  "", "';'", "'{'", "'}'", "':'", "'.'", "'['", "']'"
};

std::vector<std::string> sethemeParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "LITERAL", "STRING", "IDENTIFIER", "IDENTIFIER_WS", 
  "HEX_NUMBER", "DEC_NUMBER", "SYMBOL", "WS"
};

dfa::Vocabulary sethemeParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> sethemeParser::_tokenNames;

sethemeParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x11, 0x67, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x4, 
    0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x3, 0x2, 0x3, 0x2, 0x5, 0x2, 0x15, 
    0xa, 0x2, 0x3, 0x2, 0x3, 0x2, 0x6, 0x2, 0x19, 0xa, 0x2, 0xd, 0x2, 0xe, 
    0x2, 0x1a, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x5, 0x2, 0x20, 0xa, 0x2, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x6, 0x3, 0x26, 0xa, 0x3, 0xd, 0x3, 
    0xe, 0x3, 0x27, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x5, 0x3, 0x30, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x6, 0x4, 0x37, 0xa, 0x4, 0xd, 0x4, 0xe, 0x4, 0x38, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x41, 0xa, 
    0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 
    0x5, 0x5, 0x5, 0x4a, 0xa, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 
    0x5, 0x6, 0x50, 0xa, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x5, 0x7, 0x57, 0xa, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x6, 0x8, 
    0x5c, 0xa, 0x8, 0xd, 0x8, 0xe, 0x8, 0x5d, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 
    0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x2, 0x2, 0xa, 0x2, 
    0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x2, 0x2, 0x2, 0x6f, 0x2, 0x1f, 
    0x3, 0x2, 0x2, 0x2, 0x4, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x6, 0x40, 0x3, 0x2, 
    0x2, 0x2, 0x8, 0x49, 0x3, 0x2, 0x2, 0x2, 0xa, 0x4f, 0x3, 0x2, 0x2, 0x2, 
    0xc, 0x56, 0x3, 0x2, 0x2, 0x2, 0xe, 0x58, 0x3, 0x2, 0x2, 0x2, 0x10, 
    0x5f, 0x3, 0x2, 0x2, 0x2, 0x12, 0x15, 0x5, 0x4, 0x3, 0x2, 0x13, 0x15, 
    0x5, 0x6, 0x4, 0x2, 0x14, 0x12, 0x3, 0x2, 0x2, 0x2, 0x14, 0x13, 0x3, 
    0x2, 0x2, 0x2, 0x15, 0x16, 0x3, 0x2, 0x2, 0x2, 0x16, 0x17, 0x7, 0x3, 
    0x2, 0x2, 0x17, 0x19, 0x3, 0x2, 0x2, 0x2, 0x18, 0x14, 0x3, 0x2, 0x2, 
    0x2, 0x19, 0x1a, 0x3, 0x2, 0x2, 0x2, 0x1a, 0x18, 0x3, 0x2, 0x2, 0x2, 
    0x1a, 0x1b, 0x3, 0x2, 0x2, 0x2, 0x1b, 0x1c, 0x3, 0x2, 0x2, 0x2, 0x1c, 
    0x1d, 0x7, 0x2, 0x2, 0x3, 0x1d, 0x20, 0x3, 0x2, 0x2, 0x2, 0x1e, 0x20, 
    0x7, 0x2, 0x2, 0x3, 0x1f, 0x18, 0x3, 0x2, 0x2, 0x2, 0x1f, 0x1e, 0x3, 
    0x2, 0x2, 0x2, 0x20, 0x3, 0x3, 0x2, 0x2, 0x2, 0x21, 0x22, 0x5, 0xa, 
    0x6, 0x2, 0x22, 0x25, 0x7, 0x4, 0x2, 0x2, 0x23, 0x26, 0x5, 0x6, 0x4, 
    0x2, 0x24, 0x26, 0x5, 0x8, 0x5, 0x2, 0x25, 0x23, 0x3, 0x2, 0x2, 0x2, 
    0x25, 0x24, 0x3, 0x2, 0x2, 0x2, 0x26, 0x27, 0x3, 0x2, 0x2, 0x2, 0x27, 
    0x25, 0x3, 0x2, 0x2, 0x2, 0x27, 0x28, 0x3, 0x2, 0x2, 0x2, 0x28, 0x29, 
    0x3, 0x2, 0x2, 0x2, 0x29, 0x2a, 0x7, 0x5, 0x2, 0x2, 0x2a, 0x30, 0x3, 
    0x2, 0x2, 0x2, 0x2b, 0x2c, 0x5, 0xa, 0x6, 0x2, 0x2c, 0x2d, 0x7, 0x4, 
    0x2, 0x2, 0x2d, 0x2e, 0x7, 0x5, 0x2, 0x2, 0x2e, 0x30, 0x3, 0x2, 0x2, 
    0x2, 0x2f, 0x21, 0x3, 0x2, 0x2, 0x2, 0x2f, 0x2b, 0x3, 0x2, 0x2, 0x2, 
    0x30, 0x5, 0x3, 0x2, 0x2, 0x2, 0x31, 0x32, 0x5, 0xa, 0x6, 0x2, 0x32, 
    0x36, 0x7, 0x4, 0x2, 0x2, 0x33, 0x34, 0x5, 0x8, 0x5, 0x2, 0x34, 0x35, 
    0x7, 0x3, 0x2, 0x2, 0x35, 0x37, 0x3, 0x2, 0x2, 0x2, 0x36, 0x33, 0x3, 
    0x2, 0x2, 0x2, 0x37, 0x38, 0x3, 0x2, 0x2, 0x2, 0x38, 0x36, 0x3, 0x2, 
    0x2, 0x2, 0x38, 0x39, 0x3, 0x2, 0x2, 0x2, 0x39, 0x3a, 0x3, 0x2, 0x2, 
    0x2, 0x3a, 0x3b, 0x7, 0x5, 0x2, 0x2, 0x3b, 0x41, 0x3, 0x2, 0x2, 0x2, 
    0x3c, 0x3d, 0x5, 0xa, 0x6, 0x2, 0x3d, 0x3e, 0x7, 0x4, 0x2, 0x2, 0x3e, 
    0x3f, 0x7, 0x5, 0x2, 0x2, 0x3f, 0x41, 0x3, 0x2, 0x2, 0x2, 0x40, 0x31, 
    0x3, 0x2, 0x2, 0x2, 0x40, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x41, 0x7, 0x3, 
    0x2, 0x2, 0x2, 0x42, 0x43, 0x5, 0xa, 0x6, 0x2, 0x43, 0x44, 0x7, 0x6, 
    0x2, 0x2, 0x44, 0x45, 0x5, 0xc, 0x7, 0x2, 0x45, 0x4a, 0x3, 0x2, 0x2, 
    0x2, 0x46, 0x47, 0x5, 0xa, 0x6, 0x2, 0x47, 0x48, 0x7, 0x6, 0x2, 0x2, 
    0x48, 0x4a, 0x3, 0x2, 0x2, 0x2, 0x49, 0x42, 0x3, 0x2, 0x2, 0x2, 0x49, 
    0x46, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x9, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x50, 
    0x7, 0xc, 0x2, 0x2, 0x4c, 0x50, 0x7, 0xd, 0x2, 0x2, 0x4d, 0x50, 0x5, 
    0xe, 0x8, 0x2, 0x4e, 0x50, 0x5, 0x10, 0x9, 0x2, 0x4f, 0x4b, 0x3, 0x2, 
    0x2, 0x2, 0x4f, 0x4c, 0x3, 0x2, 0x2, 0x2, 0x4f, 0x4d, 0x3, 0x2, 0x2, 
    0x2, 0x4f, 0x4e, 0x3, 0x2, 0x2, 0x2, 0x50, 0xb, 0x3, 0x2, 0x2, 0x2, 
    0x51, 0x57, 0x7, 0xb, 0x2, 0x2, 0x52, 0x57, 0x7, 0xf, 0x2, 0x2, 0x53, 
    0x57, 0x7, 0xe, 0x2, 0x2, 0x54, 0x57, 0x7, 0xa, 0x2, 0x2, 0x55, 0x57, 
    0x5, 0xe, 0x8, 0x2, 0x56, 0x51, 0x3, 0x2, 0x2, 0x2, 0x56, 0x52, 0x3, 
    0x2, 0x2, 0x2, 0x56, 0x53, 0x3, 0x2, 0x2, 0x2, 0x56, 0x54, 0x3, 0x2, 
    0x2, 0x2, 0x56, 0x55, 0x3, 0x2, 0x2, 0x2, 0x57, 0xd, 0x3, 0x2, 0x2, 
    0x2, 0x58, 0x5b, 0x7, 0xc, 0x2, 0x2, 0x59, 0x5a, 0x7, 0x7, 0x2, 0x2, 
    0x5a, 0x5c, 0x7, 0xc, 0x2, 0x2, 0x5b, 0x59, 0x3, 0x2, 0x2, 0x2, 0x5c, 
    0x5d, 0x3, 0x2, 0x2, 0x2, 0x5d, 0x5b, 0x3, 0x2, 0x2, 0x2, 0x5d, 0x5e, 
    0x3, 0x2, 0x2, 0x2, 0x5e, 0xf, 0x3, 0x2, 0x2, 0x2, 0x5f, 0x60, 0x7, 
    0xc, 0x2, 0x2, 0x60, 0x61, 0x7, 0x7, 0x2, 0x2, 0x61, 0x62, 0x7, 0xc, 
    0x2, 0x2, 0x62, 0x63, 0x7, 0x8, 0x2, 0x2, 0x63, 0x64, 0x7, 0xc, 0x2, 
    0x2, 0x64, 0x65, 0x7, 0x9, 0x2, 0x2, 0x65, 0x11, 0x3, 0x2, 0x2, 0x2, 
    0xe, 0x14, 0x1a, 0x1f, 0x25, 0x27, 0x2f, 0x38, 0x40, 0x49, 0x4f, 0x56, 
    0x5d, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

sethemeParser::Initializer sethemeParser::_init;
