
// Generated from setheme.g4 by ANTLR 4.9

#pragma once


#include "antlr.h"
#include "sethemeListener.h"


/**
 * This class provides an empty implementation of sethemeListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class ANTLR4CPP_STATIC sethemeBaseListener : public sethemeListener {
public:

  virtual void enterSetheme(sethemeParser::SethemeContext * /*ctx*/) override { }
  virtual void exitSetheme(sethemeParser::SethemeContext * /*ctx*/) override { }

  virtual void enterGroup(sethemeParser::GroupContext * /*ctx*/) override { }
  virtual void exitGroup(sethemeParser::GroupContext * /*ctx*/) override { }

  virtual void enterObj(sethemeParser::ObjContext * /*ctx*/) override { }
  virtual void exitObj(sethemeParser::ObjContext * /*ctx*/) override { }

  virtual void enterPair(sethemeParser::PairContext * /*ctx*/) override { }
  virtual void exitPair(sethemeParser::PairContext * /*ctx*/) override { }

  virtual void enterKey(sethemeParser::KeyContext * /*ctx*/) override { }
  virtual void exitKey(sethemeParser::KeyContext * /*ctx*/) override { }

  virtual void enterValue(sethemeParser::ValueContext * /*ctx*/) override { }
  virtual void exitValue(sethemeParser::ValueContext * /*ctx*/) override { }

  virtual void enterTyped(sethemeParser::TypedContext * /*ctx*/) override { }
  virtual void exitTyped(sethemeParser::TypedContext * /*ctx*/) override { }

  virtual void enterTyped_link(sethemeParser::Typed_linkContext * /*ctx*/) override { }
  virtual void exitTyped_link(sethemeParser::Typed_linkContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

