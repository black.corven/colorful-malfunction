
// Generated from setheme.g4 by ANTLR 4.9

#pragma once


#include "antlr.h"
#include "sethemeParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by sethemeParser.
 */
class ANTLR4CPP_STATIC sethemeListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterSetheme(sethemeParser::SethemeContext *ctx) = 0;
  virtual void exitSetheme(sethemeParser::SethemeContext *ctx) = 0;

  virtual void enterGroup(sethemeParser::GroupContext *ctx) = 0;
  virtual void exitGroup(sethemeParser::GroupContext *ctx) = 0;

  virtual void enterObj(sethemeParser::ObjContext *ctx) = 0;
  virtual void exitObj(sethemeParser::ObjContext *ctx) = 0;

  virtual void enterPair(sethemeParser::PairContext *ctx) = 0;
  virtual void exitPair(sethemeParser::PairContext *ctx) = 0;

  virtual void enterKey(sethemeParser::KeyContext *ctx) = 0;
  virtual void exitKey(sethemeParser::KeyContext *ctx) = 0;

  virtual void enterValue(sethemeParser::ValueContext *ctx) = 0;
  virtual void exitValue(sethemeParser::ValueContext *ctx) = 0;

  virtual void enterTyped(sethemeParser::TypedContext *ctx) = 0;
  virtual void exitTyped(sethemeParser::TypedContext *ctx) = 0;

  virtual void enterTyped_link(sethemeParser::Typed_linkContext *ctx) = 0;
  virtual void exitTyped_link(sethemeParser::Typed_linkContext *ctx) = 0;


};

