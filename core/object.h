#pragma once

#include <string>
#include <memory>
#include <set>

#include "pair.h"
#include "Holder.h"

namespace colormalfu
{
  /**
   * Class represents setheme object
   */
  class Object : public Holder
  {
  public:
    //! C'tor by identifier
    Object(std::string identifier) : Holder(identifier) {}

    //! Insert pair via move semantic
    bool insert(std::shared_ptr<Pair>&& pair)
    {
      return Holder::insert(std::dynamic_pointer_cast<BaseProperty>(pair));
    }

    //! Insert pair by link
    bool insert(std::weak_ptr<Pair> link)
    {
      if (!link.expired())
      {
        return Holder::insert(std::dynamic_pointer_cast<BaseProperty>(link.lock()));
      }
      return false;
    }

    //! Find pair by identifier
    std::weak_ptr<Pair> findPair(std::string identifier)
    {
      auto prop = find(identifier);
      if (!prop.expired())
      {
        return std::dynamic_pointer_cast<Pair>(prop.lock());
      }
      return std::weak_ptr<Pair>();
    }

    void write(std::ofstream& of, uint32_t depth = 0);
    void write_begin(std::ofstream& of, uint32_t depth = 0);
    void write_end(std::ofstream& of, uint32_t depth = 0);
  };
}