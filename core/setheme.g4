// Copyright Tin Kesaro 2020
// Lexeer based on json5.g4

// Define a grammar for Serious Editor 4 theme format
grammar setheme;

setheme : ((group|obj)';')+ EOF | EOF;

group
  : key '{' (obj|pair)+ '}'
  | key '{' '}'
  ;

obj
  : key '{' (pair ';')+ '}'
  | key '{' '}'
  ;
  
pair
  : key ':' value
  | key ':'
  ;
  
key 
  : IDENTIFIER 
  | IDENTIFIER_WS       // There is object key : "Invalid variable"
  | typed
  | typed_link
  ;

value
   : STRING
   | DEC_NUMBER
   | HEX_NUMBER
   | LITERAL
   | typed            // pair -> Font:Font.Default_Fixed;
   ;
   
typed : IDENTIFIER ('.' IDENTIFIER)+;
typed_link : IDENTIFIER '.' IDENTIFIER '[' IDENTIFIER ']';


////////////////////////////////////////////////////////////////////
// Lexer

LITERAL
   : 'true'
   | 'false'
   //| 'null'
   ;

STRING
   : '"' DOUBLE_QUOTE_CHAR* '"'
   | '\'' SINGLE_QUOTE_CHAR* '\''
   ;

////////////////////////////////////////////////////////
// Identifier

IDENTIFIER
   : IDENTIFIER_START IDENTIFIER_PART*
   ;

IDENTIFIER_WS
   : IDENTIFIER_START (IDENTIFIER_PART)* (' ' (IDENTIFIER_PART)+)*
   ;

fragment IDENTIFIER_START
   : [\p{L}]
   | '$'
   | '_'
   | '\\' UNICODE_SEQUENCE
   ;

fragment IDENTIFIER_PART
   : IDENTIFIER_START
   | [\p{M}]
   | [\p{N}]
   | [\p{Pc}]
   | '\u200C'
   | '\u200D'
   ;
////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
// String 
fragment DOUBLE_QUOTE_CHAR
   : ~["\\\r\n]
   | ESCAPE_SEQUENCE
   ;

fragment SINGLE_QUOTE_CHAR
   : ~['\\\r\n]
   | ESCAPE_SEQUENCE
   ;

fragment ESCAPE_SEQUENCE
   : '\\'
   ( NEWLINE
   | UNICODE_SEQUENCE       // \u1234
   | ['"\\/bfnrtv]          // single escape char
   | ~['"\\bfnrtv0-9xu\r\n] // non escape char
   | '0'                    // \0
   | 'x' HEX HEX            // \x3a
   )
   ;
   
fragment UNICODE_SEQUENCE
   : 'u' HEX HEX HEX HEX
   ;   
//////////////////////////////////////////////////////// 
   
////////////////////////////////////////////////////////
// Numbers
HEX_NUMBER
  : '0' [xX] HEX+           // 0x12345678
  ;

DEC_NUMBER
   : SYMBOL? ( 
   INT ('.' [0-9]*)? EXP? // +1.e2, 1234, 1234.5
   | '.' [0-9]+ EXP?        // -.2e3
   )
   ;
 
SYMBOL : '+' | '-';

fragment HEX
   : [0-9a-fA-F]
   ;

fragment INT
   : '0' | [1-9] [0-9]*
   ;

fragment EXP
   : [Ee] SYMBOL? [0-9]*
   ;
////////////////////////////////////////////////////////

fragment NEWLINE
   : '\r\n'
   | [\r\n\u2028\u2029]
   ;

WS
   : [ \t\n\r\u00A0\uFEFF\u2003] + -> skip
   ;
