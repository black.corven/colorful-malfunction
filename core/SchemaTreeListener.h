#pragma once

#include "generated/sethemeBaseListener.h"

#include "Schema.h"

/**
 * A listener used to generate Schema by parsing tree produced by sethemeParser
 */
class SchemaTreeListener : public sethemeBaseListener
{
private:
  int32_t m_depth = 0;    //!< Curent depth in tree
  std::ofstream m_log;    //!< Logger TODO!
  
  //! Generated Schema
  std::shared_ptr<colormalfu::Schema> m_schema = std::make_shared<colormalfu::Schema>();

  //! Group whose context is currently being read
  std::shared_ptr<colormalfu::Group> curentGroup = nullptr;

  //! The stack of Objects which are currently being read
  std::stack<std::shared_ptr<colormalfu::Object>> curentObjectStack{};

  //! Pair which holds current object
  std::shared_ptr<colormalfu::Pair> curentObjectPair = nullptr;

  //! Tab function for logger
  void printTabs()
  {
    int i = m_depth;
    while (i > 0)
    {
      m_log << ' ';
      i--;
    }
  }

public:
  
  //! Get schema
  std::weak_ptr<colormalfu::Schema> getSchema()
  {
    return m_schema;
  }

  void visitErrorNode(antlr4::tree::ErrorNode* node) override
  {
    m_log << "Error Node!\n";
    m_log << node->toString();
  }

  //! Begin file event
  void enterSetheme(sethemeParser::SethemeContext* ctx) override
  {
    m_depth++;
    m_log.open("listener.log");
  }

  //! End file event
  void exitSetheme(sethemeParser::SethemeContext* ctx) override
  {
    m_depth--; m_log.close();
  }

  //! On enter group
  void enterGroup(sethemeParser::GroupContext* ctx) override
  {
    auto group = std::make_shared<colormalfu::Group>(ctx->key()->getText());
    m_schema->insert(group);
    curentGroup = group;

    printTabs();
    m_depth++;
    m_log << "Group " << group->getIdentifier() << '\n';
  }

  //! On leave group
  void exitGroup(sethemeParser::GroupContext* ctx) override
  {
    curentGroup = nullptr;

    m_depth--;
    printTabs();
    m_log << "/Group\n";
  }

  void enterObj(sethemeParser::ObjContext* ctx) override
  {
    printTabs();
    m_depth++;
    auto object = std::make_shared<colormalfu::Object>(ctx->key()->getText());

    if (curentObjectPair)
    {
      curentObjectPair->setValue(object);
      curentObjectPair = nullptr;
    }
    else if (curentGroup)
    {
      curentGroup->insert(object);
    }
    else 
    {
      m_schema->insert(object);
    }
    curentObjectStack.push(object);

    m_log << "Object " << object->getIdentifier() << '\n';
  }

  void exitObj(sethemeParser::ObjContext* ctx) override
  {
    m_depth--;
    printTabs();

    curentObjectStack.pop();

    m_log << "/Object\n";
  }


  colormalfu::Pair::ValueType conv(int tree_value_type)
  {
    colormalfu::Pair::ValueType res = colormalfu::Pair::ValueType::STRING;
    switch (tree_value_type)
    {
    case sethemeParser::DEC_NUMBER:
      res = colormalfu::Pair::ValueType::NUMBER;
      break;
    case sethemeParser::HEX_NUMBER:
      res = colormalfu::Pair::ValueType::HEX;
      break;
    case sethemeParser::LITERAL:
      res = colormalfu::Pair::ValueType::LITERAL;
      break;
    case sethemeParser::STRING:
      res = colormalfu::Pair::ValueType::STRING;
      break;
    case sethemeParser::IDENTIFIER:
    case sethemeParser::IDENTIFIER_WS:
      res = colormalfu::Pair::ValueType::IDENTIFIER;
      break;
    }
    return res;
  }

  void enterPair(sethemeParser::PairContext* ctx) override
  {
    printTabs();
    auto key = ctx->key()->getText();
    std::shared_ptr<colormalfu::Pair> pair = nullptr;

    // For pair hold object
    //if (auto p_obj = ctx->obj(); p_obj)
    //{
    //  m_log << "Pair " << " : " << key << " : " << '\n';

    //  auto pair = std::make_shared<colormalfu::Pair>(key);
    //  curentObjectPair = pair;

    //  if (!curentObjectStack.empty())
    //  {
    //    curentObjectStack.top()->insert(pair);
    //  }
    //  else if (curentGroup)
    //  {
    //    curentGroup->insert(pair);
    //  }

    //  return;
    //}

    if (auto p_value = ctx->value(); p_value)
    {
      
      if (auto p_typed = p_value->typed(); p_typed)
      {
        auto value = p_typed->getText();
        pair = std::make_shared<colormalfu::Pair>(key, colormalfu::Pair::ValueType::IDENTIFIER, value);
      }
      else
      {
        auto type = p_value->getStart()->getType();
        auto value = p_value->getText();

        switch (type)
        {
          case sethemeParser::DEC_NUMBER:
            pair = std::make_shared<colormalfu::Pair>(key, conv(type), std::stoi(value));
            break;
          case sethemeParser::HEX_NUMBER:
            pair = std::make_shared<colormalfu::Pair>(key, conv(type), std::stoul(value, nullptr, 16));
            break;
          case sethemeParser::LITERAL:
          case sethemeParser::STRING:
          case sethemeParser::IDENTIFIER:
          case sethemeParser::IDENTIFIER_WS:
            pair = std::make_shared<colormalfu::Pair>(key, conv(type), value);
            break;
          default:
            m_log << "Pair " << " : " << key << " : Unexpected type";
        }
      }
    } else {
      // Expected empty pair
      pair = std::make_shared<colormalfu::Pair>(key, colormalfu::Pair::ValueType::STRING, "");
    }

    assert(pair && "Can't create pair");  // so pair not null

    if (!curentObjectStack.empty())
    {
      curentObjectStack.top()->insert(pair);
    }
    else if (curentGroup)
    {
      curentGroup->insert(pair);
    }
  }
};