#pragma once

#include <string>
#include <fstream>

namespace colormalfu
{
  /**
   * Base class that represents named property
   */
  class BaseProperty
  {
  protected:
    std::string m_key = "UNDEFINED";    //!< Identifier, name of property

    void writeTabs(std::ofstream& of, int depth)
    {
      while (depth > 0)
      {
        of << "  ";
        depth--;
      }
    }

  public:

    //! Default c'tor
    BaseProperty(std::string identifier) : m_key(identifier) {};

    //! Get property name
    std::string getIdentifier() { return m_key; }

    //! Less by key
    bool operator<(BaseProperty const& rhs) const
    {
      // All properties sorted lexicaly
      return m_key < rhs.m_key;
    }

    //! Write key as tree node to stream
    virtual void write(std::ofstream& of, uint32_t depth = 0)
    {
      writeTabs(of, depth);
      of << m_key << ".\n";
    }

    //! Write begin segment
    virtual void write_begin(std::ofstream& of, uint32_t depth = 0)
    {
      writeTabs(of, depth);
      of << m_key;
    }

    //! Write end segment
    virtual void write_end(std::ofstream& of, uint32_t depth = 0)
    {
    }
  };

  /**
   * LessThan comparator by key
   */
  struct ComparatorProperty_ptr
  {
    using is_transparent = void;

    bool operator() (std::shared_ptr<BaseProperty> const& lhs, std::shared_ptr<BaseProperty> const& rhs) const
    {
      return *(lhs) < *(rhs);
    }

    bool operator() (std::string const& lhs, std::shared_ptr<BaseProperty> const& rhs) const
    {
      return lhs < rhs->getIdentifier();
    }

    bool operator() (std::shared_ptr<BaseProperty> const& lhs, std::string const& rhs) const
    {
      return lhs->getIdentifier() < rhs;
    }
  };
}
