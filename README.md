# Colorful Malfunction
Theme editor for the Serious editor 4.
With 4th version of SE we finally have themes (yay!). But, unfortunately, the style options are not structured, and every theme change requires the editor to be reloaded.
So, There is a desire to create a structured and adaptive design for this problem.  

February-March 2021

## What is done?

### Lua scripts highlighting.
![](https://thumbs.gfycat.com/LinedEnchantingHydatidtapeworm-size_restricted.gif)
### IO
Since `*.setheme` format is unknown, a grammar for ANTLR has been written. Theme is read using the generated parser and written back according to the parse tree.

## Install
NOT ready yet.

## Dependencies
QT, QtnProperty, Antlr4.

## Support
Sometimes it's hard to relate style options with affected GUI components. So..
