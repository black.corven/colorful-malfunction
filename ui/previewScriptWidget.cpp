#include "previewScriptWidget.h"

//! List of supported fonts
const std::vector<const char*> fonts =
{
  "",                                                       // UNDEFINED
  "font-family:Courier New; font-size:10pt;",               // DEFAULT_FIXED
  "font-family:Tahoma; font-size:8pt;",                     // DEFAULT_NORMAL
  "font-family:Tahoma; font-size:8pt;",                     // DEFAULT_BOLD,
  "font-family:Lucida Console; font-size:8pt;",             // DEFAULT_CONSOLE,

  // Actualy this three are Roboto variations, but i don't wanna add ttf as resource
  "font-family:Lucida Console; font-size:50pt;",            // DEFAULT_FT_FIXED,
  "font-family:Lucida Console; font-size:50pt;",            // DEFAULT_FT_NORMAL,
  "font-family:Lucida Console; font-size:50pt;",            // DEFAULT_FT_BOLD,

  "font-family:Lucida Console; font-size:50pt;",            // DEFAULT_FT_CONSOLE,
};

//! Preview lua script
const char* example_script =
"<common_text>"
"RunHandled<operator>(</operator>WaitForever<operator>,</operator>"
"<br>&nbsp;&nbsp;"
"OnEvery<operator>(</operator>Event<operator>(</operator><valid_variable>worldInfo</valid_variable><operator>.</operator>PlayerBorn<operator>)),</operator>"
" <keyword>function</keyword><operator>(</operator>payload<operator>)</operator>"
"<hint_comment> -- payload : CPlayerBornScriptEvent</hint_comment>"
"<br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<keyword>local</keyword> player = <hinted_variable>payload</hinted_variable><operator>:</operator>GetBornPlayer<operator>()</operator>"
"<hint_comment> -- player : CPlayerPuppetEntity</hint_comment>"
"<br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<comment>-- above: hinted variable, hint comment, keyword</comment>"
"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<valid_variable>modelValid</valid_variable><operator>:</operator>Disappear<operator>()</operator> "
"<comment> -- entity variable</comment>"
"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<selected_valid_variable>modelValidSelected</selected_valid_variable><operator>:</operator>Appear<operator>()</operator>"
"<comment> -- selected entity variable</comment>"
"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<invalid_variable>modelDeleted</invalid_variable><operator>:</operator>DisableCollision<operator>()</operator>"
"<comment> -- invalid entity variable</comment>"
"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<keyword>local</keyword> sum <operator>=</operator> <number>3</number> <operator>+</operator> <number>5</number>"
"<comment> -- number, operator</comment>"
"<br>&nbsp;&nbsp;&nbsp;&nbsp;"
"<keyword>local</keyword> str <operator>=</operator> <string>\"Hello world, \"</string> "
"<operator>..</operator> sum <operator>..</operator> <string>\" times!\"</string>"
"<comment> -- string</comment>"
"<br>&nbsp;&nbsp;"
"<keyword>end</keyword>"
"<br>"
"<operator>)</operator>"
"</common_text>";


PreviewScriptWidget::PreviewScriptWidget(QWidget* parent) :
  QWidget(parent),
  m_layout{ this },
  m_styleTags{
    { "common_text",              QColor("#FF000000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "comment",                  QColor("#FF008000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "keyword",                  QColor("#FF0000FF"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "expression",               QColor("#FF800080"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "operator",                 QColor("#FF800000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "bracket",                  QColor("#FF000080"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "number",                   QColor("#FF800080"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "string",                   QColor("#FFA31515"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "valid_variable",           QColor("#FF000080"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "invalid_variable",         QColor("#FFFF0000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "function",                 QColor("#FF880000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "macro_program",            QColor("#FF804000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "event",                    QColor("#FFFF00FF"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "this_hint",                QColor("#FFFF8000"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "object",                   QColor("#FF408040"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "error",                    QColor("#FFFF0000"), QColor("#FFFFE795"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "selected_valid_variable",  QColor("#FFFFFFFF"), QColor("#FF008000"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "disabled_Text",            QColor("#FF808080"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "hinted_variable",          QColor("#FF408080"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
    { "hint_comment",             QColor("#FF484680"), QColor("#FFFFFFFF"), false, false, ScriptFonts::DEFAULT_FIXED},
  }
{
  m_edit.setReadOnly(true);
  m_edit.setWordWrapMode(QTextOption::NoWrap);
  m_edit.setAutoFillBackground(true);

  // Disable anti-aliasing on widget, cos croteam does not use one
  auto font = m_edit.font();
  QFont().setStyleStrategy(QFont::NoAntialias);
  m_edit.setFont(font);

  m_layout.addWidget(&m_edit);
  redraw();
}

void PreviewScriptWidget::redraw()
{
  QString style = "<style>";
  for (int i = 0; i <m_styleTags.size(); i++)
  {
    style += generateTag((ScriptTag)i);
  }
  style += "</style>\n";

  m_edit.setHtml(style + example_script);

  // If you want to see an actual html..
  //m_edit.setPlainText(style + example_script);
}

void PreviewScriptWidget::setStyle(ScriptColors id, QColor color)
{
  switch (id)
  {
  case ScriptColors::NORMAL_TEXT:
    m_pal.setColor(QPalette::Text, color);
    setTagColor(ScriptTag::COMMON_TEXT, color);
    break;
  case ScriptColors::NORMAL_BACK:
    m_pal.setColor(QPalette::Base, color);
    break;
  case ScriptColors::SELECTED_TEXT:
    m_pal.setColor(QPalette::HighlightedText, color);
    break;
  case ScriptColors::SELECTED_BACK:
    m_pal.setColor(QPalette::Highlight, color);
    break;
  }
  m_edit.setPalette(m_pal);
  m_edit.repaint();
}

QString PreviewScriptWidget::generateTag(ScriptTag tag)
{
  const char* customStyleTag =
    "%1{"
    " color:%2;"
    " background-color:%3;"
    " font-style:%4;"
    " font-weight:%5;"
    " %6;"
    "}";

  auto& params = m_styleTags[(int)tag];

  auto fontId = ScriptFonts::UNDEFINED;
  if (m_forceFont != ScriptFonts::UNDEFINED) {
    fontId = m_forceFont;
  } else {
    fontId = params.font;
  }

  auto weight = 400;  // normal
  if (params.bold && (fontId == ScriptFonts::DEFAULT_BOLD || fontId == ScriptFonts::DEFAULT_FT_BOLD))
  {
    weight = 900;     // extra bold
  } 
  else if (params.bold || fontId == ScriptFonts::DEFAULT_BOLD || fontId == ScriptFonts::DEFAULT_FT_BOLD)
  {
    weight = 500;     // light bold
  }


  return QString(customStyleTag).arg
  (
    params.name,
    params.color.name(QColor::HexArgb),
    params.background.name(QColor::HexArgb),
    (params.italic) ? "italic" : "normal",
    QString::number(weight),
    fonts[ ((int)fontId < fonts.size())? (int)fontId : 0 ]
  );
}

void PreviewScriptWidget::setTag(ScriptTag tag, QColor color, QColor background, bool italic, bool bold, ScriptFonts font)
{
  auto& params = m_styleTags[(int)tag];
  params.color = color;
  params.background = background;
  params.italic = italic;
  params.bold = bold;
  params.font = font;
  //redraw();
}

void PreviewScriptWidget::setTagColor(ScriptTag tag, QColor color)
{
  m_styleTags[(int)tag].color = color;
}

void PreviewScriptWidget::setTagBackground(ScriptTag tag, QColor background)
{
  m_styleTags[(int)tag].background = background;
}

void PreviewScriptWidget::setTagItalic(ScriptTag tag, bool italic)
{
  m_styleTags[(int)tag].italic = italic;
}

void PreviewScriptWidget::setTagBold(ScriptTag tag, bool bold)
{
  m_styleTags[(int)tag].bold = bold;
}

void PreviewScriptWidget::setTagFont(ScriptTag tag, ScriptFonts font)
{
  m_styleTags[(int)tag].font = font;
}

void PreviewScriptWidget::setForceFont(ScriptFonts font)
{
  m_forceFont = font;
}
