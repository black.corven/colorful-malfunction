#include "previewMainEditorWidget.h"

#include <QMenuBar>

PreviewMainEditorWidget::PreviewMainEditorWidget(QWidget* parent)
  :
  QWidget(parent),
  m_layout{ this },
  m_statusBar{ this }
{
  QMenuBar* previewMenu = new QMenuBar(this);
  auto m = new QMenu("File", previewMenu);
  previewMenu->addMenu(m);

  m_layout.addWidget(previewMenu);
  m_layout.setAlignment(previewMenu, Qt::AlignmentFlag::AlignTop);

  m_layout.addWidget(&m_statusBar);
  m_statusBar.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  m_layout.setAlignment(&m_statusBar, Qt::AlignmentFlag::AlignBottom);

  redraw();
}