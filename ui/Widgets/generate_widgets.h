#pragma once

#include "../main_window.h"
#include "../previewScriptWidget.h"
#include "QtnProperty/PropertyCore.h"
#include "QtnProperty/PropertyWidget.h"
#include "../Properties/DelegateQColorArgb.h"
#include "../Properties/DelegateSEFont.h"
#include "../Properties/PropertySEFont.h"

///////////////////////////////////////////////////////////////////////////////
// Property widgets

enum class PropertyType
{
  UNDEFINED = 0,
  COLOR_ARGB,
  SE_FONT,
  STRING,
};

//! Generate properties for window general property widget
QWidget* genWindowGeneralPropertyWidget(main_window* owner);

//! Generate properties for script property widget
QWidget* genScriptPropertyWidget(main_window* owner);

// Generate widgets routines
QWidget* genWindowColorsPropertyWidget(main_window* owner);

// Get all color properties from Schema
void createColorProperty(QtnPropertySet* propertySet, std::shared_ptr<colormalfu::BaseProperty>& elem);

//! Find pair in schema by path in Property
std::shared_ptr<colormalfu::BaseProperty> getByProperty(const colormalfu::Schema* schema, const QtnPropertyBase* p);

//! Find pair in schema by list of inheritance
std::shared_ptr<colormalfu::BaseProperty> getByPath(const colormalfu::Schema* schema, const std::list<std::string>& p);


///////////////////////////////////////////////////////////////////////////////
// Preview widgets

//! Create preview widget for script
inline QWidget* genScriptPreviewWidget(main_window* owner)
{
  return new PreviewScriptWidget();
}


