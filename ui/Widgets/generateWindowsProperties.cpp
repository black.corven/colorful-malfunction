#include "generate_widgets.h"


QtnPropertySet* genStatusBarPropertySet(main_window* owner)
{
  auto previewWidget = owner->getPreviewMap()->find(ItemIndex::WINDOW_STATUS_BAR)->second;

  auto ps = new QtnPropertySet();
  ps->setName("status_bar");
  ps->setDisplayName("Status Bar");
  return ps;
}


QWidget* genWindowGeneralPropertyWidget(main_window* owner)
{
  auto previewWidget = owner->getPreviewMap()->find(ItemIndex::WINDOW_GENERAL)->second;

  auto ps = new QtnPropertySet(owner);
  auto generalParams = qtnCreateProperty<QtnPropertySet>(ps, "general");
  generalParams->setDisplayName("General");

  auto statusBarParams = genStatusBarPropertySet(owner);
  statusBarParams->setParent(ps);
  ps->addChildProperty(statusBarParams);

  auto propertyWidget = new QtnPropertyWidget();
  propertyWidget->setPropertySet(ps);
  propertyWidget->setParts(QtnPropertyWidgetPartsDescriptionPanel);
  return propertyWidget;
}