#pragma once

#include <QLabel>
#include <QStatusBar>
#include <QBoxLayout>

#include <QMenu>
#include <QAction>

#include <QPropertyAnimation>

/*!
 * QLabel with background-color as QProperty
 */
class AnimatableLabel : public QLabel
{
  Q_OBJECT
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)

private:
  QColor m_defaultBack = QColor("gray");

public:
  AnimatableLabel(QWidget* parent = 0): QLabel(parent) {}

  void setDefaultBack(QColor color)
  {
    m_defaultBack = color;
    setBackgroundColor(m_defaultBack);
  }

  void setBackgroundColor(QColor color)
  {
    setStyleSheet(QString("background-color: %1;").arg(color.name(QColor::HexArgb)));
  }

  QColor backgroundColor() { return m_defaultBack; }
};

/*!
 * Widget that reproduce the apperiance of a Serious Editor's status bar.
 */
class PreviewStatusBarWidget : public QFrame
{
  Q_OBJECT

private:
  const QString labelStyle = "QLabel { background-color: %1; }";
  const QString statusBarStyle =
    "PreviewStatusBarWidget { background-color: %2; }"
    "PreviewStatusBarWidget QLabel { color: %1; border: 1px solid %3; }";
  
  // Widgets
  QHBoxLayout m_layout;
  QLabel m_messageLabel;
  QLabel m_entitiesLabel;
  QLabel m_gridLabel;
  AnimatableLabel m_errorLabel;

  // Error and warning animations
  QPropertyAnimation m_errorAnimation;
  QPropertyAnimation m_warningAnimation;

  // GLobal parameters?
  QColor m_borderColor = QColor("black");
  QColor m_textColor = QColor("black");
  QColor m_backColor = QColor("green");
  
  // Parameters
  QColor m_entitiesColor = QColor("#FFB2D2FF");
  QColor m_gridColor = QColor("#FFB9B9B4");
  QColor m_errorColor = QColor("#FFFFC0C0");
  QColor m_warningColor = QColor("#FFFFFFC0");

public:
  //! Default c'tor
  PreviewStatusBarWidget(QWidget* parent = nullptr) :
    QFrame(parent),
    m_layout{ this },
    m_errorAnimation{ &m_errorLabel, "backgroundColor" },
    m_warningAnimation{ &m_errorLabel, "backgroundColor" }
  {
    m_messageLabel.setFrameStyle(QFrame::Panel | QFrame::Sunken);
    m_messageLabel.setText(QString("Very useful message..."));
    m_messageLabel.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    
    m_gridLabel.setFrameStyle(QFrame::Panel | QFrame::Sunken);
    m_gridLabel.setText(QString("Grid: 1m         "));

    m_entitiesLabel.setFrameStyle(QFrame::Box | QFrame::Sunken);
    m_entitiesLabel.setText(QString("1 entities                         "));
   
    m_errorLabel.setFrameStyle(QFrame::Box | QFrame::Sunken);
    m_errorLabel.setText(QString("0 error(s), 0 warning(s)"));
    m_errorLabel.setDefaultBack(m_gridColor);
    m_errorLabel.setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);

    QMenu* newMenu = new QMenu(this);
    auto actionOnError = new QAction("Error", &m_errorLabel);
    auto actionOnWarning = new QAction("Warning", &m_errorLabel);
    auto actionOnDefault = new QAction("Default", &m_errorLabel);

    newMenu->addAction(actionOnError);
    newMenu->addAction(actionOnWarning);
    newMenu->addAction(actionOnDefault);
    
    connect(&m_errorLabel, &AnimatableLabel::customContextMenuRequested, 
      [label = &m_errorLabel, newMenu](const QPoint a_pos) {
        newMenu->exec(label->mapToGlobal(a_pos));
      });

    connect(actionOnError, &QAction::triggered,
      [this]() {
        m_errorLabel.setText(QString("1 error(s), 0 warning(s)"));
        m_warningAnimation.stop();
        m_errorAnimation.start();
      });

    connect(actionOnWarning, &QAction::triggered,
      [this]() {
        m_errorLabel.setText(QString("0 error(s), 1 warning(s)"));
        m_errorAnimation.stop();
        m_warningAnimation.start();
      });

    connect(actionOnDefault, &QAction::triggered,
      [this]() {
        m_errorLabel.setText(QString("0 error(s), 0 warning(s)"));
        m_errorAnimation.stop();
        m_warningAnimation.stop();
        m_errorLabel.setBackgroundColor(m_errorLabel.backgroundColor());
      });

    m_errorAnimation.setDuration(400);
    m_errorAnimation.setStartValue(m_gridColor);
    m_errorAnimation.setKeyValueAt(0.5f, m_errorColor);
    m_errorAnimation.setEndValue(m_gridColor);
    m_errorAnimation.setLoopCount(-1);

    m_warningAnimation.setDuration(400);
    m_warningAnimation.setStartValue(m_gridColor);
    m_warningAnimation.setKeyValueAt(0.5f, m_warningColor);
    m_warningAnimation.setEndValue(m_gridColor);
    m_warningAnimation.setLoopCount(-1);

    m_layout.setContentsMargins(0, 2, 0, 0);
    m_layout.setSpacing(2);
    m_layout.addWidget(&m_messageLabel);
    m_layout.addWidget(&m_gridLabel);
    m_layout.addWidget(&m_entitiesLabel);
    m_layout.addWidget(&m_errorLabel);

    setAutoFillBackground(true);
    setFrameStyle(QFrame::NoFrame | QFrame::Sunken);
    //setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    redraw();
  }
  
  // Parameters setters
  void setEntitiesColor(QColor color) { m_entitiesColor = color; }
  void setGridColor(QColor color) { m_gridColor = color; }
  void setErrorColor(QColor color) { m_errorColor = color; }
  void setWarningColor(QColor color) { m_warningColor = color; }

public slots:
  //! Apply params to apperiance.
  void redraw()
  {
    m_messageLabel.setStyleSheet(labelStyle.arg(
      m_gridColor.name(QColor::HexArgb)
    ));

    m_entitiesLabel.setStyleSheet(labelStyle.arg(
      m_entitiesColor.name(QColor::HexArgb)
    ));

    m_gridLabel.setStyleSheet(labelStyle.arg(
      m_gridColor.name(QColor::HexArgb)
    ));

    this->setStyleSheet(statusBarStyle.arg(
      m_textColor.name(QColor::HexArgb),
      m_backColor.name(QColor::HexArgb),
      m_borderColor.name(QColor::HexArgb)
    ));
  }

};