#pragma once

//#include <QWidget>
#include <QLabel>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QStatusBar>

#include "previewStatusBarWidget.h"

/**
 * Widget for preview main editor window with all kind of components.
 */
class PreviewMainEditorWidget : public QWidget
{
  Q_OBJECT

private:
  QVBoxLayout m_layout;                //!< Layout
  PreviewStatusBarWidget m_statusBar;

public:
  //! Default c'tor
  PreviewMainEditorWidget(QWidget* parent = nullptr);

public slots:
  void redraw() {}

};