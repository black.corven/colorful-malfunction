﻿#include "generate_widgets.h"

// Get all color properties from Schema
void createColorProperty(QtnPropertySet* propertySet, std::shared_ptr<colormalfu::BaseProperty>& elem)
{
  auto id = QString::fromStdString(elem->getIdentifier());

  // if it's holder
  if (auto holder = std::dynamic_pointer_cast<colormalfu::Holder>(elem); holder)
  {
    // Color Group
    auto childSet = qtnCreateProperty<QtnPropertySet>(propertySet);
    childSet->setName(id);
    holder->for_each([childSet](auto& el) { createColorProperty(childSet, el); });
  }

  // if it's pair
  else if (auto pair = std::dynamic_pointer_cast<colormalfu::Pair>(elem); pair)
  {
    // Color Property
    auto textColor = qtnCreateProperty<QtnPropertyQColor>(propertySet);
    textColor->setName(id);
    QColor color = QColor(QString::fromStdString("#" + pair->getValueString())); //#AARRGGBB
    textColor->setValue(color);
    textColor->setDelegateInfoCallback([]() -> QtnPropertyDelegateInfo {
      QtnPropertyDelegateInfo info;
      info.name = "ARGB_COLOR";
      info.attributes["shape"] = QtnColorDelegateShapeCircle; // For aplha channel rendering
      info.attributes["rgbSubItems"] = true;
      return info;
      });

    QObject::connect(textColor, &QtnProperty::propertyDidChange,
      [textColor, pair](QtnPropertyChangeReason reason)
      {
        if (reason & QtnPropertyChangeReasonValue) {
          auto str = (textColor)->value().name(QColor::HexArgb).toStdString(); //#AARRGGBB
          pair->setValue(str.substr(1)); //AARRGGBB
        }
      });

    textColor->collapse();
  }
};

QWidget* genWindowColorsPropertyWidget(main_window* owner)
{
  auto propertyWidget = new QtnPropertyWidget();
  propertyWidget->setPropertySet(nullptr);
  return propertyWidget;
}