#include "generate_widgets.h"

#include <qmessagebox.h>

std::shared_ptr<colormalfu::BaseProperty> getByPath(const colormalfu::Schema* schema, const std::list<std::string>& path)
{
  if (auto link = schema->find_recursive(path); !link.expired())
  {
    return link.lock();
  }
  return nullptr;
}

std::shared_ptr<colormalfu::BaseProperty> getByProperty(const colormalfu::Schema* schema, const QtnPropertyBase* p)
{
  if (!schema || !p){
    return nullptr;
  }

  // well, that's smelly
  std::list<std::string> path;
  for (auto& str : p->delegateInfo()->attributes["propertyPath"].toStringList())
  {
    path.push_back(str.toStdString());
  }

  auto baseProperty = getByPath(schema, path);
  if (!baseProperty)
  {
    // TODO: Log can't find pair by path
    QMessageBox::information(
      nullptr,
      "Warning",
      QString("Theme does not contain property '%1' with path '%2'").arg(
        p->name(),
        p->delegateInfo()->attributes["propertyPath"].toStringList().join('/') // WARN: possible bug?
      )
    );
  }
  return baseProperty;
}

//! Params for Qtn property
struct PropertyParams
{
  std::string id;
  std::string name;
  std::string description;
  std::list<std::string> path;
};

// Create color property by params and connect it with model
QtnPropertyQColor* addArgbColorProperty(
  main_window* schema_holder, QtnPropertySet* set,
  const PropertyParams& params, const QString& value) 
{
  auto colorProperty = qtnCreateProperty<QtnPropertyQColor>(set);
  colorProperty->setName(QString::fromStdString(params.id));
  colorProperty->setDisplayName(QString::fromStdString(params.name));
  colorProperty->setDescription(QString::fromStdString(params.description));
  colorProperty->setValue(QColor(value));  //#AARRGGBB

  QStringList list;
  for (auto& s : params.path)
  {
    list << QString::fromStdString(s);
  }

  colorProperty->setDelegateInfoCallback(
    [list]() -> QtnPropertyDelegateInfo {
      QtnPropertyDelegateInfo info;
      info.name = "ARGB_COLOR";
      info.attributes["shape"] = QtnColorDelegateShapeCircle; // For aplha channel rendering
      info.attributes["rgbSubItems"] = true;
      info.attributes["propertyPath"] = list;
      info.attributes["propertyType"] = (int)PropertyType::COLOR_ARGB;
      return info;
    });

  QObject::connect(colorProperty, &QtnProperty::propertyDidChange,
    [schema_holder, colorProperty, path = params.path](QtnPropertyChangeReason reason)
  {
    // Value changed
    if (reason & QtnPropertyChangeReasonValue)
    {
      auto str = (colorProperty)->value().name(QColor::HexArgb).toStdString(); //#AARRGGBB
      if (auto schema = schema_holder->getSchema(); schema)
      {
        if (auto pair = std::dynamic_pointer_cast<colormalfu::Pair>(getByPath(schema_holder->getSchema(), path)); pair)
        {
          pair->setValue(str.substr(1)); //AARRGGBB
        }
      }
    }
  });

  colorProperty->collapse();
  return colorProperty;
}

PropertySEFont* addFontProperty(main_window* schema_holder, QtnPropertySet* set, const PropertyParams& params)
{
  auto property = qtnCreateProperty<PropertySEFont>(set);
  property->setName(QString::fromStdString(params.id));
  property->setDisplayName(QString::fromStdString(params.name));
  property->setDescription(QString::fromStdString(params.description));

  QStringList list;
  for (auto& s : params.path)
  {
    list << QString::fromStdString(s);
  }
  
  property->setDelegateInfoCallback(
    [list]() -> QtnPropertyDelegateInfo {
      QtnPropertyDelegateInfo info;
      info.name = "SE_FONT";
      // info.attributes["shape"] = QtnColorDelegateShapeCircle; // For aplha channel rendering
      // info.attributes["rgbSubItems"] = true;
      info.attributes["propertyPath"] = list;
      info.attributes["propertyType"] = (int)PropertyType::SE_FONT;
      return info;
    });

  property->collapse();

  QObject::connect(property, &QtnProperty::propertyDidChange,
    [schema_holder, property, path = params.path](QtnPropertyChangeReason reason)
  {
    // Value changed
    if (reason & QtnPropertyChangeReasonValue)
    {
      struct Param {
        std::string id;
        std::string val;
        colormalfu::Pair::ValueType type;
      };
      std::vector<Param> vals;

      auto val = (property)->value();
      vals.push_back({ "Ink", val.getColor().name(QColor::HexArgb).toStdString().substr(1), colormalfu::Pair::ValueType::STRING });
      vals.push_back({ "Paper", val.getBackground().name(QColor::HexArgb).toStdString().substr(1), colormalfu::Pair::ValueType::STRING });
      vals.push_back({ "Italic", std::to_string((int)val.getItalic()), colormalfu::Pair::ValueType::NUMBER});
      vals.push_back({ "Bold", std::to_string((int)val.getBold()), colormalfu::Pair::ValueType::NUMBER });
      vals.push_back({ "Font", val.getFont().toStdString(), colormalfu::Pair::ValueType::STRING });

      // TODO: Callback to fill schema
      if (auto schema = schema_holder->getSchema(); schema)
      {
        if (auto obj = std::dynamic_pointer_cast<colormalfu::Object>(getByPath(schema_holder->getSchema(), path)); obj)
        {
          for (auto& v : vals)
          {
            if (auto l = obj->findPair(v.id); !l.expired()) {
              l.lock()->setValue(v.val);
            } else {
              obj->insert(std::make_shared<colormalfu::Pair>("Pair Weak", v.type, v.val));
            }
          }
        }
      }
    }
  });

  return property;
}


/**
 * Functor for ARGB color property. Connect property with preview widget.
 */
class callbackScriptColorProperty
{
  QtnPropertyQColor* m_property;      //!< Pointer to property to get cur value
  ScriptColors m_tag;                 //!< Associated tag in preview widget
  PreviewScriptWidget* m_pw;          //!< Pointer to preview widget

public:
  callbackScriptColorProperty(QtnPropertyQColor* colorProperty, const ScriptColors tag, PreviewScriptWidget* pw) :
    m_property(colorProperty), m_tag(tag), m_pw(pw) 
  {
  }

  void operator() (QtnPropertyChangeReason reason)
  {
    if ((reason & QtnPropertyChangeReasonValue) && m_pw)
    {
      m_pw->setStyle(m_tag, m_property->value());
      m_pw->redraw();
    }
  }
};


ScriptFonts getFontFromString(QString str)
{
  return (ScriptFonts)(PropertySEFontBase::supportedSEfonts.indexOf(str) + 1);
}

/**
 * Functor for PropertySEFont. Connect property with preview widget.
 */
class callbackScriptFontProperty
{
  PropertySEFont* m_property;         //!< Pointer to property to get cur value
  ScriptTag m_tag;                    //!< Associated tag in preview widget
  PreviewScriptWidget* m_pw;          //!< Pointer to preview widget

public:
  callbackScriptFontProperty(PropertySEFont* fontProperty, const ScriptTag tag, PreviewScriptWidget* pw) :
    m_property(fontProperty), m_tag(tag), m_pw(pw) {}

  void operator() (QtnPropertyChangeReason reason)
  {
    if ((reason & QtnPropertyChangeReasonValue) && m_pw)
    {
      auto sefont = m_property->value();
      m_pw->setTag(
        m_tag,
        sefont.getColor(),
        sefont.getBackground(),
        sefont.getItalic(),
        sefont.getBold(),
        getFontFromString(sefont.getFont())
      );
      m_pw->redraw();
    }
  }
};

QWidget* genScriptPropertyWidget(main_window* owner)
{
  auto previewWidget = owner->getPreviewMap()->find(ItemIndex::SCRIPT)->second;

  auto scriptParams = new QtnPropertySet(owner);
  auto generalParams = qtnCreateProperty<QtnPropertySet>(scriptParams, "general");
  generalParams->setDisplayName("General");

  auto highlightParams = qtnCreateProperty<QtnPropertySet>(scriptParams, "highlight");
  highlightParams->setDisplayName("Highlighting");

  auto outdatedParams = qtnCreateProperty<QtnPropertySet>(scriptParams, "obsolete");
  outdatedParams->setDisplayName("Obsolete");
  outdatedParams->collapse();

  /////////////////////////////////////////////////////////////////////////////
  // General

  {
    auto textColor = addArgbColorProperty(owner, generalParams,
      {
        "NormalTextColor",
        "Generic text color",
        "Color of generic text in script editor",
        { "TextControl", "NormalTextColor" }
      }, "#FF000000");
    QObject::connect(textColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(textColor, ScriptColors::NORMAL_TEXT, (PreviewScriptWidget*)previewWidget));
    emit textColor->propertyDidChange(QtnPropertyChangeReasonNewValue);

    auto backgroundColor = addArgbColorProperty((main_window*)owner, generalParams,
      {
        "NormalBackgroundColor",
        "Background color",
        "Generic background color in script editor",
        { "TextControl", "NormalBackgroundColor" }
      }, "#FFFFFFFF");
    QObject::connect(backgroundColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(backgroundColor, ScriptColors::NORMAL_BACK, (PreviewScriptWidget*)previewWidget));
    QObject::connect(backgroundColor, &QtnProperty::propertyDidChange, 
      [m_property = backgroundColor, set1 = highlightParams, set2 = outdatedParams]
      (QtnPropertyChangeReason reason)
      {
        if ((reason & QtnPropertyChangeReasonValue))
        {
          if (set1)
          {
            for (auto& prop : set1->childProperties())
            {
              prop->setDelegateAttribute("baseBackColor", m_property->value());
            }
          }

          if (set2)
          {
            for (auto& prop : set2->childProperties())
            {
              prop->setDelegateAttribute("baseBackColor", m_property->value());
            }
          }
        }
      });
    emit backgroundColor->propertyDidChange(QtnPropertyChangeReasonNewValue);

    auto selectedTextColor = addArgbColorProperty(owner, generalParams,
      {
        "SelectedTextColor",
        "Selected text color",
        "Color of selected text in script editor",
        { "TextControl", "SelectedTextColor" }
      }, "#FFFF0000");
    QObject::connect(selectedTextColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(selectedTextColor, ScriptColors::SELECTED_TEXT, (PreviewScriptWidget*)previewWidget));
    emit selectedTextColor->propertyDidChange(QtnPropertyChangeReasonNewValue);

    auto selectedBackgroundColor = addArgbColorProperty(owner, generalParams,
      {
        "SelectedTextBackgroundColor",
        "Selected background color",
        "Background color of selected text in script editor",
        { "TextControl", "SelectedTextBackgroundColor" }
      }, "#FFDFDFDF");
    QObject::connect(selectedBackgroundColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(selectedBackgroundColor, ScriptColors::SELECTED_BACK, (PreviewScriptWidget*)previewWidget));
    emit selectedBackgroundColor->propertyDidChange(QtnPropertyChangeReasonNewValue);

    auto similarTextBackgroundColor = addArgbColorProperty(owner, generalParams,
      {
        "SimilarTextBackgroundColor",
        "Similar text background color",
        "Background color of text similar to selected in script editor",
        { "TextControl", "SimilarTextBackgroundColor" }
      }, "#FFFFFFBF");
    QObject::connect(similarTextBackgroundColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(similarTextBackgroundColor, ScriptColors::SIMILAR_BACK, (PreviewScriptWidget*)previewWidget));
    emit similarTextBackgroundColor->propertyDidChange(QtnPropertyChangeReasonNewValue);

    auto lineNumberTextColor = addArgbColorProperty(owner, generalParams,
      {
        "LineNumberTextColor",
        "Line number color",
        "Color of number in line numbers",
        { "TextControl", "LineNumberTextColor" }
      }, "#FF2427DC");
    QObject::connect(lineNumberTextColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(lineNumberTextColor, ScriptColors::LINE_TEXT, (PreviewScriptWidget*)previewWidget));
    emit lineNumberTextColor->propertyDidChange(QtnPropertyChangeReasonNewValue);


    auto lineNumberBackColor = addArgbColorProperty(owner, generalParams,
      {
        "LineNumberBackColor",
        "Line number background color",
        "Backgrond color of line numbers",
        { "TextControl", "LineNumberBackColor" }
      }, "#FFFFFFBF");
    QObject::connect(lineNumberBackColor, &QtnProperty::propertyDidChange,
      callbackScriptColorProperty(lineNumberBackColor, ScriptColors::LINE_BACK, (PreviewScriptWidget*)previewWidget));
    emit lineNumberBackColor->propertyDidChange(QtnPropertyChangeReasonNewValue);
  }

  /////////////////////////////////////////////////////////////////////////////
  // Highlighting
  {
    auto forcedFont = qtnCreateProperty<QtnPropertyQString>(highlightParams);
    forcedFont->setName("Force Font");
    forcedFont->setDisplayName("Forced font");
    forcedFont->setDescription("Override highlighting font params");

    QtnPropertyDelegateInfo delegate;
    delegate.name = "ComboBox";
    delegate.attributes["items"] = (QStringList("") << PropertySEFont::supportedSEfonts);
    delegate.attributes["propertyPath"] = QStringList{ "ScriptSourceColorizing", "Force font" };
    delegate.attributes["propertyType"] = (int)PropertyType::STRING;
    forcedFont->setDelegateInfo(delegate);
    forcedFont->setValue("");
    QObject::connect(forcedFont, &QtnProperty::propertyDidChange,
      [schema_holder = owner, property = forcedFont](QtnPropertyChangeReason reason)
    {
      // Value changed
      if (reason & QtnPropertyChangeReasonValue)
      {
        if (auto schema = schema_holder->getSchema(); schema)
        {
          if (auto p = std::dynamic_pointer_cast<colormalfu::Pair>(getByPath(schema_holder->getSchema(), { "ScriptSourceColorizing", "Force font" })); p)
          {
            if (p->getValueString().empty() || p->getType() != colormalfu::Pair::ValueType::IDENTIFIER)
            {
              *p = colormalfu::Pair("Force font", colormalfu::Pair::ValueType::IDENTIFIER, (property)->value().toStdString());
            } else {
              p->setValue((property)->value().toStdString());
            }
            
          }
        }
      }
    });
    QObject::connect(forcedFont, &QtnProperty::propertyDidChange,
      [m_property = forcedFont, m_pw = (PreviewScriptWidget*)previewWidget]
    (QtnPropertyChangeReason reason)
    {
        if ((reason & QtnPropertyChangeReasonValue) && m_pw)
        {
          m_pw->setForceFont(getFontFromString(m_property->value()));
          m_pw->redraw();
        }
      }
    );

    auto commonText = addFontProperty(owner, highlightParams,
      {
        "Common text",
        "Common text",
        "Generic script text",
        { "ScriptSourceColorizing", "Common text" }
      });
    QObject::connect(commonText, &QtnProperty::propertyDidChange, 
      callbackScriptFontProperty(commonText, ScriptTag::COMMON_TEXT, (PreviewScriptWidget*)previewWidget));
    commonText->collapse();


    auto keyword = addFontProperty(owner, highlightParams,
      {
        "Keyword",
        "Keyword",
        "Lua keywords like if, else, function etc",
        { "ScriptSourceColorizing", "Keyword" }
      });
    QObject::connect(keyword, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(keyword, ScriptTag::KEYWORD, (PreviewScriptWidget*)previewWidget));
    keyword->collapse();


    auto comment = addFontProperty(owner, highlightParams,
      {
        "Comment",
        "Comment",
        "Single line comments",
        { "ScriptSourceColorizing", "Comment" }
      });
    QObject::connect(comment, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(comment, ScriptTag::COMMENT, (PreviewScriptWidget*)previewWidget));
    comment->collapse();

    auto op = addFontProperty(owner, highlightParams,
      {
        "Operator",
        "Operator",
        "Any sign, bracket, etc",
        { "ScriptSourceColorizing", "Operator" }
      });
    QObject::connect(op, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(op, ScriptTag::OPERATOR, (PreviewScriptWidget*)previewWidget));
    op->collapse();

    auto number = addFontProperty(owner, highlightParams,
      {
        "Number",
        "Number",
        "Any digit in script",
        { "ScriptSourceColorizing", "Number" }
      });
    QObject::connect(number, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(number, ScriptTag::NUMBER, (PreviewScriptWidget*)previewWidget));
    number->collapse();

    auto str = addFontProperty(owner, highlightParams,
      {
        "String",
        "String",
        "Any quoted string",
        { "ScriptSourceColorizing", "String" }
      });
    QObject::connect(str, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(str, ScriptTag::STRING, (PreviewScriptWidget*)previewWidget));
    str->collapse();

    auto valid_variable = addFontProperty(owner, highlightParams,
      {
        "Valid_variable",
        "Valid variable",
        "Any variable linked to entity",
        { "ScriptSourceColorizing", "Valid variable" }
      });
    QObject::connect(valid_variable, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(valid_variable, ScriptTag::VALID_VARIABLE, (PreviewScriptWidget*)previewWidget));
    valid_variable->collapse();

    auto invalid_variable = addFontProperty(owner, highlightParams,
      {
        "Invalid_variable",
        "Invalid variable",
        "Any variable which linked entity has been deleted",
        { "ScriptSourceColorizing", "Invalid variable" }
      });
    QObject::connect(invalid_variable, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(invalid_variable, ScriptTag::INVALID_VARIABLE, (PreviewScriptWidget*)previewWidget));
    invalid_variable->collapse();

    auto selected_valid_variable = addFontProperty(owner, highlightParams,
      {
        "Selected_valid_variable",
        "Selected valid variable",
        "Variable linked to selected entity",
        { "ScriptSourceColorizing", "Selected valid variable" }
      });
    QObject::connect(selected_valid_variable, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(selected_valid_variable, ScriptTag::SELECTED_VALID_VARIABLE, (PreviewScriptWidget*)previewWidget));
    selected_valid_variable->collapse();

    auto hinted_variable = addFontProperty(owner, highlightParams,
      {
        "Hinted_variable",
        "Hinted variable",
        "Variable TODO!", // TODO
        { "ScriptSourceColorizing", "Hinted variable" }
      });
    QObject::connect(hinted_variable, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(hinted_variable, ScriptTag::HINTED_VARIABLE, (PreviewScriptWidget*)previewWidget));
    hinted_variable->collapse();

    auto hint_comment = addFontProperty(owner, highlightParams,
      {
        "Hint_comment",
        "Hint comment",
        "Hint comment associates a variable with a given class, enabling auto-completion for this variable. "
          "It only exists to ease the workflow of writing scripts and does not affect performing code.",
        { "ScriptSourceColorizing", "Hint comment" }
      });
    QObject::connect(hint_comment, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(hint_comment, ScriptTag::HINT_COMMENT, (PreviewScriptWidget*)previewWidget));
    hint_comment->collapse();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Obsolete

  {

    auto expression = addFontProperty(owner, outdatedParams,
    {
      "Expression",
      "Expression",
      " ",
      { "ScriptSourceColorizing", "Expression" }
    });
    QObject::connect(expression, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(expression, ScriptTag::EXPRESSION, (PreviewScriptWidget*)previewWidget));
    expression->collapse();

    auto bracket = addFontProperty(owner, outdatedParams,
      {
        "Bracket",
        "Bracket",
        " ",
        { "ScriptSourceColorizing", "Bracket" }
      });
    QObject::connect(bracket, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(bracket, ScriptTag::BRACKET, (PreviewScriptWidget*)previewWidget));
    bracket->collapse();

    auto function = addFontProperty(owner, outdatedParams,
      {
        "Function",
        "Function",
        " ",
        { "ScriptSourceColorizing", "Function" }
      });
    QObject::connect(function, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(function, ScriptTag::FUNCTION, (PreviewScriptWidget*)previewWidget));
    function->collapse();

    auto macro_progamm = addFontProperty(owner, outdatedParams,
      {
        "Macro_program",
        "Macro program",
        " ",
        { "ScriptSourceColorizing", "Macro program" }
      });
    QObject::connect(macro_progamm, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(macro_progamm, ScriptTag::MACRO_PROGRAM, (PreviewScriptWidget*)previewWidget));
    macro_progamm->collapse();

    auto eventProperty = addFontProperty(owner, outdatedParams,
      {
        "Event",
        "Event",
        " ",
        { "ScriptSourceColorizing", "Event" }
      });
    QObject::connect(eventProperty, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(eventProperty, ScriptTag::EVENT, (PreviewScriptWidget*)previewWidget));
    eventProperty->collapse();

    auto this_hint = addFontProperty(owner, outdatedParams,
      {
        "This_hint",
        "This hint",
        " ",
        { "ScriptSourceColorizing", "This hint" }
      });
    QObject::connect(this_hint, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(this_hint, ScriptTag::THIS_HINT, (PreviewScriptWidget*)previewWidget));
    this_hint->collapse();

    auto object = addFontProperty(owner, outdatedParams,
      {
        "Object",
        "Object",
        " ",
        { "ScriptSourceColorizing", "Object" }
      });
    QObject::connect(object, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(object, ScriptTag::OBJECT, (PreviewScriptWidget*)previewWidget));
    object->collapse();

    auto error = addFontProperty(owner, outdatedParams,
      {
        "Error",
        "Error",
        " ",
        { "ScriptSourceColorizing", "Error" }
      });

    QObject::connect(error, &QtnProperty::propertyDidChange,
      callbackScriptFontProperty(error, ScriptTag::ERROR, (PreviewScriptWidget*)previewWidget));
    error->collapse();
  }

  auto pw = new QtnPropertyWidget();
  pw->setPropertySet(scriptParams);
  pw->setParts(QtnPropertyWidgetPartsDescriptionPanel);
  return pw;
}
