#pragma once

#include <QWidget>
#include <QTextEdit>
#include <QVBoxLayout>

enum class ScriptFonts
{
  UNDEFINED = 0,
  DEFAULT_FIXED,
  DEFAULT_NORMAL,
  DEFAULT_BOLD,
  DEFAULT_CONSOLE,
  DEFAULT_FT_FIXED,
  DEFAULT_FT_NORMAL,
  DEFAULT_FT_BOLD,
  DEFAULT_FT_CONSOLE,
};

enum class ScriptTag
{
  COMMON_TEXT = 0,
  COMMENT,
  KEYWORD,
  EXPRESSION,
  OPERATOR,
  BRACKET,
  NUMBER,
  STRING,
  VALID_VARIABLE,
  INVALID_VARIABLE,
  FUNCTION,
  MACRO_PROGRAM,
  EVENT,
  THIS_HINT,
  OBJECT,
  ERROR,
  SELECTED_VALID_VARIABLE,
  DISABLED_TEXT,
  HINTED_VARIABLE,
  HINT_COMMENT,
};

enum class ScriptColors
{
  NORMAL_TEXT = 0,
  NORMAL_BACK,
  SELECTED_TEXT,
  SELECTED_BACK,
  SIMILAR_BACK,
  LINE_TEXT,
  LINE_BACK,
};

/**
 * Widget for preview Serious engine 3.5+ lua script highlighting
 */
class PreviewScriptWidget : public QWidget
{
  Q_OBJECT

public:
  //! Default c'tor
  PreviewScriptWidget(QWidget* parent = nullptr);

public slots:

  //! Redraw script preview
  void redraw();

  //! Change parameters of specified tag
  void setTag(ScriptTag tag, QColor color, QColor background, bool italic, bool bold, ScriptFonts font);
  void setTagColor(ScriptTag tag, QColor color);
  void setTagBackground(ScriptTag tag, QColor background);
  void setTagItalic(ScriptTag tag, bool italic);
  void setTagBold(ScriptTag tag, bool bold);
  void setTagFont(ScriptTag tag, ScriptFonts font);
  void setForceFont(ScriptFonts font);

  //! Change widget style
  void setStyle(ScriptColors id, QColor color);

private:
  struct Tag
  {
    QString name;
    QColor color;
    QColor background;
    bool italic;
    bool bold;
    ScriptFonts font;
  };

  QPalette m_pal;                     //! Widget palette
  QTextEdit m_edit;                   //!< Display widget
  QVBoxLayout m_layout;               //!< Layout
  std::vector<Tag> m_styleTags;       //!< List of style tags
  ScriptFonts m_forceFont;            //!< Forced font style

  //! Convert highlight params to style string
  QString generateTag(ScriptTag tag);
};