#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_main_window.h"

#include "../core/Schema.h"

#include <map>

//! Parameter sets enumerator
enum class ItemIndex
{
  DEFAULT = 0,
  SCRIPT,
  WINDOW,
  WINDOW_GENERAL,
  WINDOW_WIDGETS,
  WINDOW_STATUS_BAR,
  WINDOW_ALL_COLORS,
};

class main_window : public QMainWindow
{
  Q_OBJECT

public:
  main_window(QWidget* parent = Q_NULLPTR);

  colormalfu::Schema* getSchema()
  {
    // TODO: on more scheme
    if (schema)
    {
      return schema.get();
    }
    else
    {
      return nullptr;
    }
  }

  auto getPreviewMap() { return &previewWidgetMap; }
  auto getPropertyMap() { return &propertyWidgetMap; }

private:
  Ui::main_windowClass ui;
  std::shared_ptr<colormalfu::Schema> schema = nullptr;     //!< opened schema

  QWidget* curentPreviewWidget = nullptr;
  QWidget* curentPropertyWidget = nullptr;

  std::map<ItemIndex, QWidget*> previewWidgetMap;           //!< Map of preview widgets
  std::map<ItemIndex, QWidget*> propertyWidgetMap;          //!< Map of property widgets

  QString m_currentPath = "";

  //! Sugar for tree item add
  void addTreeChild(QTreeWidgetItem* parent, QString name, ItemIndex index);

  //! Generate content for tree of params
  void prepareParamsTree();

  //! Most of connects goes here
  void prepareActions();

  //! Generate preview and property widgets
  void prepareWidgets();

  // Get properties for current property widget dynamicaly
  void loadProperties(ItemIndex index);

public slots:

  //! Change widget content on schema load
  void on_schema_setted();

  //! Change preview and property widget on tree item clicked
  void on_treewidget_clicked(QTreeWidgetItem* item, int column);
};
