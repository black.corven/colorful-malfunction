#include "PropertyQColorArgbHandler.h"

PropertyQColorArgbHandler::PropertyQColorArgbHandler(
  QtnPropertyDelegate* delegate, QtnLineEditBttn& editor)
  : QtnPropertyEditorHandlerType(delegate, editor)
{
  if (!stateProperty()->isEditableByUser())
  {
    editor.lineEdit->setReadOnly(true);
    editor.toolButton->setEnabled(false);
  }

  updateEditor();
  editor.lineEdit->installEventFilter(this);
  QObject::connect(editor.toolButton, &QToolButton::clicked, this,
    &PropertyQColorArgbHandler::onToolButtonClicked);
  QObject::connect(editor.lineEdit, &QLineEdit::editingFinished, this,
    &PropertyQColorArgbHandler::onEditingFinished);
}

void PropertyQColorArgbHandler::onToolButtonClick()
{
  onToolButtonClicked(false); 
}

void PropertyQColorArgbHandler::updateEditor()
{
  QString str = property().value().name(QColor::HexArgb);
  editor().setTextForProperty(stateProperty(), str);
  editor().lineEdit->selectAll();
}

void PropertyQColorArgbHandler::onToolButtonClicked(bool)
{
  auto property = &this->property();
  volatile bool destroyed = false;
  auto connection = QObject::connect(property, &QObject::destroyed,
    [&destroyed]() mutable { destroyed = true; });
  reverted = true;
  auto dialog = new QColorDialog(property->value(), editorBase());
  auto dialogContainer = connectDialog(dialog);

  // To Show alpha chanel in dialog
  dialog->setOption(QColorDialog::ShowAlphaChannel, true);

  if (dialog->exec() == QDialog::Accepted && !destroyed)
  {
    property->setValue(dialog->currentColor(), delegate()->editReason());
  }

  if (!destroyed)
    QObject::disconnect(connection);

  Q_UNUSED(dialogContainer);
}

void PropertyQColorArgbHandler::onEditingFinished()
{
  if (canApply())
  {
    property().setValue(
      QColor(editor().lineEdit->text()), delegate()->editReason());
  }

  applyReset();
}
