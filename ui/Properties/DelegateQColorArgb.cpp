#include "DelegateQColorArgb.h"

#include "PropertyQColorArgbHandler.h"

#include "QtnProperty/Delegates/PropertyDelegateFactory.h"
#include "QtnProperty/PropertyDelegateAttrs.h"
#include "QtnProperty/Utils/InplaceEditing.h"

DelegateQColorArgb::DelegateQColorArgb(QtnPropertyQColorBase& owner)
  : QtnPropertyDelegateQColor(owner)
{
}

void DelegateQColorArgb::Register()
{
  QtnPropertyDelegateFactory::staticInstance(). // Get default factory
    registerDelegate(
      &QtnPropertyQColorBase::staticMetaObject,
      &qtnCreateDelegate<DelegateQColorArgb, QtnPropertyQColorBase>,
      QByteArrayLiteral("ARGB_COLOR"));
}

// Hack for alpha property
QtnProperty* createAlphaProperty(QtnPropertyQColorBase& owner)
{
  auto result = owner.createFieldProperty(
    &QColor::alpha, 
    &QColor::setAlpha,
    QStringLiteral("alpha"),
    "Alpha",
    "Alpha component of %1"
  );

  result->setMinValue(0);
  result->setMaxValue(255);

  QtnPropertyDelegateInfo delegate;
  delegate.name = qtnSliderBoxDelegate();
  delegate.attributes[qtnLiveUpdateAttr()] = true;
  delegate.attributes[qtnAnimateAttr()] = true;
  delegate.attributes[qtnFillColorAttr()] = QColor(152, 152, 152);
  result->setDelegateInfo(delegate);
  return result;
}

void DelegateQColorArgb::applyAttributesImpl(const QtnPropertyDelegateInfo& info)
{
  QtnPropertyDelegateQColor::applyAttributesImpl(info);

  // Add additional alpha property
  if (info.getAttribute(qtnRgbSubItemsAttr(), false))
  {
    addSubProperty(createAlphaProperty(owner()));
  }
}

//void DelegateQColorArgb::drawValueImpl(QStylePainter& painter, const QRect& rect) const
//{
//
//}

QWidget* DelegateQColorArgb::createValueEditorImpl(
  QWidget* parent,
  const QRect& rect, 
  QtnInplaceInfo* inplaceInfo)
{
  QtnLineEditBttn* editor = new QtnLineEditBttn(parent);
  editor->setGeometry(rect);

  // QColorDialog with alpha channel
  new PropertyQColorArgbHandler(this, *editor);

  if (inplaceInfo)
  {
    editor->lineEdit->selectAll();
  }

  return editor;
}

bool DelegateQColorArgb::propertyValueToStrImpl(QString& strValue) const
{
  // Show as #AARRGGBB
  strValue = owner().value().name(QColor::HexArgb);
  return true; // TODO: check on err
}
