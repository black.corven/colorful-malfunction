#include "QtnProperty/Delegates/PropertyDelegateFactory.h"
#include "DelegateSEFont.h"
#include<QDebug>


DelegateSEFont::DelegateSEFont(PropertySEFontBase& owner)
  : QtnPropertyDelegateTypedEx<PropertySEFontBase>(owner)
{
}

void DelegateSEFont::Register()
{
  QtnPropertyDelegateFactory::staticInstance(). // Get default factory
    registerDelegateDefault(
      &PropertySEFontBase::staticMetaObject,
      &qtnCreateDelegate<DelegateSEFont, PropertySEFontBase>,
      QByteArrayLiteral("SE_FONT"));
}

void DelegateSEFont::applyAttributesImpl(const QtnPropertyDelegateInfo& info)
{
  auto owner = &this->owner();
  addSubProperty(owner->createColorProperty());
  addSubProperty(owner->createBackgroundProperty());
  addSubProperty(owner->createItalicProperty());
  addSubProperty(owner->createBoldProperty());
  addSubProperty(owner->createFontProperty());
}

void DelegateSEFont::drawValueImpl(QStylePainter& painter, const QRect& rect) const
{
  if (stateProperty()->isMultiValue())
  {
    QtnPropertyDelegateTypedEx::drawValueImpl(painter, rect);
    return;
  }

  SEFont value = owner().value();

  QRect textRect = rect;

  if (textRect.isValid())
  {
    textRect.adjust(2, 2, -2, -2);
    auto back = owner().delegateInfo()->getAttribute<QColor>("baseBackColor", QColor(255, 255, 255, 255));
    painter.fillRect(textRect, back);
    painter.fillRect(textRect, value.getBackground());
    
    QFont tmpFont = painter.font();
    QPen tmpPen = painter.pen();

    painter.setPen(QPen(value.getColor()));
    painter.setFont(QFont("Arial", 10, (value.getBold()) ? QFont::Bold : QFont::Weight::Normal, value.getItalic()));
    painter.drawText(textRect, "Test Text", QTextOption(Qt::AlignCenter));

    painter.setFont(tmpFont);
    painter.setPen(tmpPen);

    QtnPropertyDelegateTyped<PropertySEFontBase>::drawValueImpl(painter, textRect);
	}
}

QWidget* DelegateSEFont::createValueEditorImpl(QWidget* parent, const QRect& rect, QtnInplaceInfo* inplaceInfo)
{
  return nullptr;
}

bool DelegateSEFont::propertyValueToStrImpl(QString& strValue) const
{
  return true; //owner().toStr(strValue);
}
