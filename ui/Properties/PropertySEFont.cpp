#include "PropertySEFont.h"

#include <QDebug>

#include "QtnProperty/Auxiliary/PropertyDelegateInfo.h"
#include "QtnProperty/Delegates/GUI/PropertyDelegateQColor.h"
#include "QtnProperty/Core/PropertyBool.h"
#include "QtnProperty/Core/PropertyQString.h"

QDataStream& operator<<(QDataStream& stream, const SEFont& font)
{
  stream << font.m_color;
  stream << font.m_background;
  stream << font.m_italic;
  stream << font.m_bold;
  stream << font.m_font;
  return stream;
}

QDataStream& operator>>(QDataStream& stream, SEFont& font)
{
  stream >> font.m_color;
  stream >> font.m_background;
  stream >> font.m_italic;
  stream >> font.m_bold;
  stream >> font.m_font;
  return stream;
}

PropertySEFontBase::PropertySEFontBase(QObject* parent)
  : QtnStructPropertyBase<SEFont, QtnPropertyIntCallback>(parent)
{
}

QtnProperty* PropertySEFontBase::createColorProperty()
{
  auto result = qtnCreateFieldProperty<QtnPropertyQColorCallback>(
    this,                             // Owner
    &SEFont::getColor,                // Getter
    &SEFont::setColor,                // Setter
    "Ink",                          // Name
    "Ink",                            // Display name
    "Color of text"                   // Description
  );

  QtnPropertyDelegateInfo delegate;
  delegate.name = "ARGB_COLOR";
  delegate.attributes["shape"] = QtnColorDelegateShapeCircle;
  delegate.attributes["rgbSubItems"] = true;
  //delegate.attributes[qtnLiveUpdateAttr()] = true;
  result->setDelegateInfo(delegate);
  result->collapse();
  return result;
}

QtnProperty* PropertySEFontBase::createBackgroundProperty()
{
  auto result = qtnCreateFieldProperty<QtnPropertyQColorCallback>(
    this,                             // Owner
    &SEFont::getBackground,           // Getter
    &SEFont::setBackground,           // Setter
    "Paper",                     // Name
    "Paper",                          // Display name
    "Background color of text"        // Description
  );

  QtnPropertyDelegateInfo delegate;
  delegate.name = "ARGB_COLOR";
  delegate.attributes["shape"] = QtnColorDelegateShapeCircle;
  delegate.attributes["rgbSubItems"] = true;
  //delegate.attributes[qtnLiveUpdateAttr()] = true;
  result->setDelegateInfo(delegate);
  result->collapse();
  return result;
}

QtnProperty* PropertySEFontBase::createItalicProperty()
{
  auto result = qtnCreateFieldProperty<QtnPropertyBoolCallback>(
    this,
    &SEFont::getItalic,
    &SEFont::setItalic,
    "Italic",
    "Italic",
    "Is font coursive"
  );

  QtnPropertyDelegateInfo delegate;
  delegate.attributes[qtnLiveUpdateAttr()] = true;
  result->setDelegateInfo(delegate);
  result->setValue(false);

  return result;
}

QtnProperty* PropertySEFontBase::createBoldProperty()
{
  auto result = qtnCreateFieldProperty<QtnPropertyBoolCallback>(
    this,
    &SEFont::getBold,
    &SEFont::setBold,
    "Bold",
    "Bold",
    "Is font bold"
  );

  QtnPropertyDelegateInfo delegate;
  delegate.attributes[qtnLiveUpdateAttr()] = true;
  result->setDelegateInfo(delegate);
  result->setValue(false);

  return result;
}

QStringList PropertySEFontBase::supportedSEfonts =
{
  "Font.Default_Fixed",
  "Font.Default_Normal",
  "Font.Default_Bold",
  "Font.Default_Console",
  "Font.DefaultFT_Fixed",
  "Font.DefaultFT_Normal",
  "Font.DefaultFT_Bold",
  "Font.DefaultFT_Console",
};

QtnProperty* PropertySEFontBase::createFontProperty()
{
  auto result = qtnCreateFieldProperty<QtnPropertyQStringCallback>(
    this,
    &SEFont::getFont,
    &SEFont::setFont,
    "Font",
    "Font",
    "One of supported fonts"
  );

  QtnPropertyDelegateInfo delegate;
  delegate.name = "ComboBox";
  delegate.attributes["items"] = supportedSEfonts;
  result->setDelegateInfo(delegate);
  result->setValue("Font.Default_Fixed");
  return result;
}

bool PropertySEFontBase::fromStrImpl(const QString& str, QtnPropertyChangeReason reason)
{
  SEFont font;
  return PropertySEFont::fontFromStr(str, font) && setValue(font, reason);
}

bool PropertySEFontBase::toStrImpl(QString& str) const
{
  return PropertySEFont::strFromFont(value(), str);
}

// To create property for your own structure you need a functions for the conversion from/into QString.
// Since SEFont complex structure without special format I use QStringList.

const char separator = '\n';


bool PropertySEFont::strFromFont(const SEFont& font, QString& str)
{

  QStringList list;
  list
    << font.m_color.name(QColor::HexArgb)
    << font.m_background.name(QColor::HexArgb)
    << ((font.m_italic)? "1" : "0")
    << ((font.m_bold)? "1" : "0")
    << font.m_font;
  str = list.join(separator);
  qInfo() << "FONT STR! " << str << '\n';
  return true;
}

bool PropertySEFont::fontFromStr(const QString& str, SEFont& font)
{
  QStringList list;
  list = str.split(separator);
  qInfo() << "STR FONT " << str << '\n';
  if (list.size() < 5)
  {
    return false;
  }

  QColor color(list[0]);
  QColor back(list[1]);
  bool italic = (list[2][0] == '1');
  bool bold = (list[3][0] == '1');

  if (!color.isValid() || !back.isValid())
  {
    return false;
  }

  font.m_color = color;
  font.m_bold = bold;
  font.m_background = back;
  font.m_italic = italic;
  font.m_font = list[4];

  return true;
}
