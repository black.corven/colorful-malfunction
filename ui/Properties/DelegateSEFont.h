#pragma once

#include "QtnProperty/Delegates/Utils/PropertyDelegateMisc.h"

#include "PropertySEFont.h"

class DelegateSEFont : 
  public QtnPropertyDelegateTypedEx<PropertySEFontBase>
{
  Q_DISABLE_COPY(DelegateSEFont)

public:
  DelegateSEFont(PropertySEFontBase& owner);

  static void Register();

protected: 
  virtual void applyAttributesImpl(const QtnPropertyDelegateInfo& info) override;

  virtual void drawValueImpl(QStylePainter& painter, const QRect& rect) const override;

  virtual QWidget* createValueEditorImpl(QWidget* parent, const QRect& rect, QtnInplaceInfo* inplaceInfo = nullptr) override;

  virtual bool propertyValueToStrImpl(QString& strValue) const override;

};
