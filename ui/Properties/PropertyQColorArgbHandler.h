/*******************************************************************************
Copyright (c) 2012-2016 Alex Zhondin <lexxmark.dev@gmail.com>
Copyright (c) 2015-2019 Alexandra Cherdantseva <neluhus.vagus@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Copyright(c) 2021 Tin Kesaro <>
// QtnProperty/Delegates/GUI/PropertyDelegateQColor.cpp

#include "QtnProperty/Delegates/GUI/PropertyDelegateQColor.h"
#include "QtnProperty/Delegates/Utils/PropertyEditorHandler.h"
#include "QtnProperty/Delegates/Utils/PropertyEditorAux.h"


#include <QColorDialog>

#pragma once

class PropertyQColorArgbHandler
  : public QtnPropertyEditorBttnHandler<QtnPropertyQColorBase, QtnLineEditBttn>
{
public:
  PropertyQColorArgbHandler(QtnPropertyDelegate* delegate, QtnLineEditBttn& editor);

protected:
  virtual void onToolButtonClick() override;
  virtual void updateEditor() override;

private:
  void onToolButtonClicked(bool); // 'cos signal clicked(bool)
  void onEditingFinished();
};
