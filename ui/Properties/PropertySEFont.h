#pragma once

#include "QtnProperty/Auxiliary/PropertyTemplates.h"
#include "QtnProperty/StructPropertyBase.h"
#include "QtnProperty/Core/PropertyInt.h" 


#include <QString>
#include <QColor>

/**
 * Serious Engine script font params
 */
struct SEFont
{
  QColor m_color = QColor(0, 0, 0, 255);            //!< Text color
  QColor m_background = QColor(255, 255, 255, 0);   //!< Color of text background
  bool m_italic = false;                            //!< Is coursive
  bool m_bold = false;                              //!< Is bold
  QString m_font;                                   //!< Supported font family

  // Getters
  QColor  getColor() const      { return m_color; }
  QColor  getBackground() const { return m_background; }
  bool    getItalic() const     { return m_italic; }
  bool    getBold() const       { return m_bold; }
  QString getFont() const       { return m_font; }

  // Setters
  void setColor(QColor color)           { m_color = color; }
  void setBackground(QColor background) { m_background = background; }
  void setItalic(bool italic)           { m_italic = italic; }
  void setBold(bool bold)               { m_bold = bold; }
  void setFont(const QString& font)     { m_font = font; }
};


inline bool operator == (const SEFont& left, const SEFont& right)
{ 
  // crap
  return  left.m_color == right.m_color && 
          left.m_background == right.m_background &&
          left.m_italic == right.m_italic && 
          left.m_bold == right.m_bold &&
          left.m_font == right.m_font
  ;
}
inline bool operator != (const SEFont& left, const SEFont& right) { return !(left == right); }

QDataStream& operator<<(QDataStream& stream, const SEFont& font);
QDataStream& operator>>(QDataStream& stream, SEFont& font);

Q_DECLARE_METATYPE(SEFont)

/**
 * SEFont Property base
 */
class PropertySEFontBase : public QtnStructPropertyBase<SEFont, QtnPropertyIntCallback>
{
  Q_OBJECT

private:
  PropertySEFontBase(const PropertySEFontBase& other) Q_DECL_EQ_DELETE;

public:
  static QStringList supportedSEfonts;

  explicit PropertySEFontBase(QObject* parent);

  // Sub properties
  QtnProperty* createColorProperty();
  QtnProperty* createBackgroundProperty();
  QtnProperty* createItalicProperty();
  QtnProperty* createBoldProperty();
  QtnProperty* createFontProperty();

protected:
  // string conversion implementation
  bool fromStrImpl(const QString& str, QtnPropertyChangeReason reason) override;
  bool toStrImpl(QString& str) const override;

  P_PROPERTY_DECL_MEMBER_OPERATORS(PropertySEFontBase)
};

P_PROPERTY_DECL_EQ_OPERATORS(PropertySEFontBase, SEFont);

class PropertySEFont : public QtnSinglePropertyValue<PropertySEFontBase>
{
  Q_OBJECT

private:
  PropertySEFont(const PropertySEFont& other) Q_DECL_EQ_DELETE;

public:
  explicit PropertySEFont::PropertySEFont(QObject* parent = nullptr)
    : QtnSinglePropertyValue<PropertySEFontBase>(parent)
  {}

  static bool fontFromStr(const QString& str, SEFont& color);
  static bool strFromFont(const SEFont& color, QString& str);

  P_PROPERTY_DECL_MEMBER_OPERATORS2(PropertySEFont, PropertySEFontBase)
};