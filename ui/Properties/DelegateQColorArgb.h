#pragma once

#include "QtnProperty/Delegates/GUI/PropertyDelegateQColor.h"
#include "QtnProperty/GUI/PropertyQColor.h"

/**
 * A QColor delegate with the alpha channel hacked.
 */
class DelegateQColorArgb
  : public QtnPropertyDelegateQColor
{
  Q_DISABLE_COPY(DelegateQColorArgb)

public:
  DelegateQColorArgb(QtnPropertyQColorBase& owner);

  //! Register in default in default factory
  static void Register();

protected:

  //! Delegate atributes
  virtual void applyAttributesImpl(
    const QtnPropertyDelegateInfo& info) override;

  //! Draw
  //virtual void DelegateQColorArgb::drawValueImpl(
  //  QStylePainter& painter, const QRect& rect) const override;

  //! Line Editor and Dialog 
  virtual QWidget* createValueEditorImpl(
    QWidget* parent, const QRect& rect, QtnInplaceInfo* inplaceInfo) override;

  //! Get string value of property for showing
  virtual bool propertyValueToStrImpl(QString& strValue) const override;

};