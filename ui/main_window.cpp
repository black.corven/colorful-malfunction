﻿#include "main_window.h"

//#include "../3rd_party/qtPropertyBrowser/qttreepropertybrowser.h"

#include <qfiledialog.h>
#include <qmessagebox.h>

#include "Widgets/generate_widgets.h"

#include "QtnProperty/PropertyCore.h"
#include "QtnProperty/PropertyGUI.h"
#include "QtnProperty/PropertyWidget.h"

#include "Properties/DelegateQColorArgb.h"
#include "Properties/DelegateSEFont.h"

#include <QFontDatabase>

main_window::main_window(QWidget *parent)
    : QMainWindow(parent)
{
  //QtTreePropertyBrowser tree;
  ui.setupUi(this);

  DelegateQColorArgb::Register();
  DelegateSEFont::Register();

  prepareParamsTree();
  prepareActions();
  prepareWidgets();
}

enum class ItemRole
{
  INDEX = Qt::UserRole,
  POINTER
};

void main_window::addTreeChild(QTreeWidgetItem* parent, QString name, ItemIndex index)
{
  QTreeWidgetItem* treeItem = new QTreeWidgetItem();

  treeItem->setText(0, name);
  treeItem->setData(0, (int)ItemRole::INDEX, (int)index);

  parent->addChild(treeItem);
}

// Structure and bindings of parameters tree
void main_window::prepareParamsTree()
{
  QTreeWidgetItem* scriptStyleItem = new QTreeWidgetItem();
  scriptStyleItem->setText(0, "Scripts");
  scriptStyleItem->setData(0, (int)ItemRole::INDEX, (int)ItemIndex::SCRIPT);

  QTreeWidgetItem* elementsItem = new QTreeWidgetItem();
  elementsItem->setText(0, "Windows");
  elementsItem->setData(0, (int)ItemRole::INDEX, (int)ItemIndex::WINDOW);
  addTreeChild(elementsItem, "General (WIP)", ItemIndex::WINDOW_GENERAL);

  QTreeWidgetItem* widgetsItem = new QTreeWidgetItem();
  widgetsItem->setDisabled(true);
  widgetsItem->setText(0, "Widgets");
  widgetsItem->setData(0, (int)ItemRole::INDEX, (int)ItemIndex::WINDOW_WIDGETS);
  elementsItem->addChild(widgetsItem);
  addTreeChild(widgetsItem, "Status bar", ItemIndex::WINDOW_STATUS_BAR);

  addTreeChild(elementsItem, "All colors", ItemIndex::WINDOW_ALL_COLORS);

  ui.groupTreeWidget->addTopLevelItems({ elementsItem, scriptStyleItem});
}

void main_window::prepareActions()
{
  // When clicked file Open
  connect(ui.actionOpen, &QAction::triggered,
    [=]()
    {
      QString openFileName = QFileDialog::getOpenFileName(this,
        "Open file", "",
        "Serious Editor theme (*.setheme);;All Files (*)");

      if (!openFileName.isEmpty())
      {
        m_currentPath = openFileName;
        schema = colormalfu::Schema::read(m_currentPath.toStdString());
        emit on_schema_setted();
      }
      else {
        // Err
      }
    });

  // When clicked File->Save
  connect(ui.actionSave, &QAction::triggered,
    [=]()
    {
      if (!schema)
      {
        QMessageBox::warning(this, "Ops..", "There is no opened schema!");
        return;
      }

      if(m_currentPath.isEmpty())
      {
        emit ui.actionSaveAs;
      }
      else {
        // TODO: write non strict if seted
        std::ofstream ou(m_currentPath.toStdString(), std::ios::binary);
        colormalfu::write_schema_strict(ou, schema);
      }
    });

  // When clicked File->Save As
  connect(ui.actionSaveAs, &QAction::triggered,
    [=]()
    {
      if (!schema)
      {
        QMessageBox::warning(this, "Ops..", "There is no opened schema!");
        return;
      }

      QFileDialog fileDialog(this, "Save file");
      fileDialog.setAcceptMode(QFileDialog::AcceptSave);
      QStringList filters;
      filters
        << "Serious Editor theme strict(*.setheme)"
        << "Serious Editor theme (*.setheme)"
        << "All Files (*)";
      fileDialog.setNameFilters(filters);

      if (fileDialog.exec() == QDialog::Accepted)
      {
        auto fileName = fileDialog.selectedFiles().first();
        if (!fileName.isEmpty())
        {
          m_currentPath = fileName;
          std::ofstream ou(m_currentPath.toStdString(), std::ios::binary);
          if (fileDialog.selectedNameFilter() == filters[1])
          {
            // Non strict file structure
            schema->write(ou);
          } else {
            colormalfu::write_schema_strict(ou, schema);
          }
        }
      }
    });

  // TODO: When pressed Ctrl+S

  connect(ui.groupTreeWidget, &QTreeWidget::itemClicked, this, &main_window::on_treewidget_clicked);
}

#include "Widgets/previewMainEditorWidget.h"

// Fiil map of widgets on work space
void main_window::prepareWidgets()
{
  // TODO: Fabric ENUM -> Widget; ENUM-> PropertySet

  // PREVIEW MAP
  curentPreviewWidget = ui.previewArea;
  previewWidgetMap.try_emplace(ItemIndex::DEFAULT, ui.previewArea);
  previewWidgetMap.try_emplace(ItemIndex::WINDOW_ALL_COLORS, nullptr);

  auto previewScriptWidget = genScriptPreviewWidget(this);
  previewWidgetMap.try_emplace(ItemIndex::SCRIPT, previewScriptWidget);
  auto previewMainEditorWidget = new PreviewMainEditorWidget(this);
  previewMainEditorWidget->hide();
  previewWidgetMap.try_emplace(ItemIndex::WINDOW_GENERAL, previewMainEditorWidget);

  // PROPERTY MAP
  curentPropertyWidget = ui.propertyArea;
  propertyWidgetMap.try_emplace(ItemIndex::DEFAULT, ui.propertyArea);

  auto generalWidget = genWindowGeneralPropertyWidget(this);
  generalWidget->setMinimumWidth(100);
  propertyWidgetMap.try_emplace(ItemIndex::WINDOW_GENERAL, generalWidget);

  auto colorsWidget = genWindowColorsPropertyWidget(this); 
  colorsWidget->setMinimumWidth(300);
  propertyWidgetMap.try_emplace(ItemIndex::WINDOW_ALL_COLORS, colorsWidget);

  auto scriptWidget = genScriptPropertyWidget(this);
  scriptWidget->setMinimumWidth(300);
  scriptWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
  propertyWidgetMap.try_emplace(ItemIndex::SCRIPT, scriptWidget);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Slots

// When chosen new schema
void main_window::on_schema_setted()
{
  // Remove old dinamic property set
  QtnPropertyWidget* widget = (QtnPropertyWidget*)propertyWidgetMap.find(ItemIndex::WINDOW_ALL_COLORS)->second;
  if (auto rootSet = widget->propertySet(); rootSet)
  {
    // ISSUE: [QtnProperty 2.0.3] 
    // If you use QtnPropertySet::clearChildrenProperties() which i don't understand is used for..
    // then last Property from refilled set uses default delegate and attributes and it can't be changed
    //
    // So. Provide a new property set for your new model loaded.
    delete rootSet;
    widget->setPropertySet(nullptr);
  }

  emit on_treewidget_clicked(ui.groupTreeWidget->currentItem(), 0);
}

void main_window::loadProperties(ItemIndex index)
{
  if (!schema)
  {
    // No model -> no dynamic data...
    return;
  }

  QtnPropertyWidget* pw = (QtnPropertyWidget*)propertyWidgetMap.find(index)->second;
  
  if (index == ItemIndex::SCRIPT)
  {
    auto set = pw->propertySet();
    auto general = set->findChild<QtnPropertySet*>("general");
    for (auto p : general->childProperties())
    {
      if (auto pair = std::dynamic_pointer_cast<colormalfu::Pair>(getByProperty(schema.get(), p)); pair)
      {
        // TODO: enum for propertyType
        if (p->delegateInfo()->attributes["propertyType"].toInt() == (int)PropertyType::COLOR_ARGB)
        {
          QString value = QString::fromStdString(pair->getValueString());
          p->fromStr("#" + value, QtnPropertyChangeReasonValue);
        }
      }
    }

    auto highlight = set->findChild<QtnPropertySet*>("highlight");
    for (auto p : highlight->childProperties())
    {
      auto info = p->delegateInfo();
      if (!info) {
        continue;
      }
      switch (info->attributes["propertyType"].toInt())
      {
      case (int)PropertyType::SE_FONT:
        if (auto object_font = std::dynamic_pointer_cast<colormalfu::Holder>(getByProperty(schema.get(), p)); object_font)
        {
          SEFont seFont;
          // ... could be better
          seFont.setBold(std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Bold").lock())->getValueString() == "1");
          seFont.setItalic(std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Italic").lock())->getValueString() == "1");
          seFont.setColor(QColor("#" + QString::fromStdString(
            std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Ink").lock())->getValueString()
          )));
          seFont.setBackground(QColor("#" + QString::fromStdString(
            std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Paper").lock())->getValueString()
          )));
          seFont.setFont(QString::fromStdString(
            std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Font").lock())->getValueString()
          ));
          ((PropertySEFont*)p)->setValue(seFont);
        }
        break;
      case (int)PropertyType::STRING:
        if (auto pair = std::dynamic_pointer_cast<colormalfu::Pair>(getByProperty(schema.get(), p)); pair)
        {
          ((QtnPropertyQString*)p)->setValue(QString::fromStdString(pair->getValueString()));
        }
        break;
      }
    }

    auto obsolete = set->findChild<QtnPropertySet*>("obsolete");
    for (auto p : obsolete->childProperties())
    {
      auto info = p->delegateInfo();
      if (!info) {
        continue;
      }
      if (info->attributes["propertyType"].toInt() == (int)PropertyType::SE_FONT)
      {
        if (auto object_font = std::dynamic_pointer_cast<colormalfu::Holder>(getByProperty(schema.get(), p)); object_font)
        {
          SEFont seFont;
          // ... could be better
          seFont.setBold(std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Bold").lock())->getValueString() == "true");
          seFont.setItalic(std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Italic").lock())->getValueString() == "true");
          seFont.setColor(QColor("#" + QString::fromStdString(
            std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Ink").lock())->getValueString()
          )));
          seFont.setBackground(QColor("#" + QString::fromStdString(
            std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Paper").lock())->getValueString()
          )));
          seFont.setFont(QString::fromStdString(
            std::dynamic_pointer_cast<colormalfu::Pair>(object_font->find("Font").lock())->getValueString()
          ));
          ((PropertySEFont*)p)->setValue(seFont);
        }
      }
    }
  }

  if (index == ItemIndex::WINDOW_ALL_COLORS)
  {
    // Expected that colors group contains only colors ¯\_(ツ)_/¯
    if (auto link = schema->findHolder("Colors"); !link.expired())
    {
      auto defaultSet = new QtnPropertySet(this);
      auto colors = link.lock();
      colors->for_each([defaultSet](auto& el) { createColorProperty(defaultSet, el); });
      pw->setPropertySet(defaultSet);
    }
    else
    {
      // TODO: Log that colors group not found in schema
      QMessageBox::information(
        nullptr,
        "Warning",
        QString("Theme does not contains Colors group")
      );
    }
  }
}

void main_window::on_treewidget_clicked(QTreeWidgetItem* item, int column)
{
  
  auto index = (ItemIndex)item->data(0, (int)ItemRole::INDEX).toInt();

  // Find widget for preview
  if (auto it = previewWidgetMap.find(index); it != previewWidgetMap.end())
  {
    curentPreviewWidget->hide();
    if (it->second)
    {
      ui.horizontalLayout->replaceWidget(curentPreviewWidget, it->second);
      curentPreviewWidget = it->second;
      curentPreviewWidget->show(); // if was hidden
    }
  }

  // Find widget for properties
  if (auto it = propertyWidgetMap.find(index); it != propertyWidgetMap.end())
  {
    curentPropertyWidget->hide();
    if (it->second)
    {
      ui.horizontalLayout->replaceWidget(curentPropertyWidget, it->second);
      curentPropertyWidget = it->second;
      loadProperties(index);
      curentPropertyWidget->show();
    }
  }
}
